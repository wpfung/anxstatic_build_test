angular.module('app', ['anxApp', 'anxApp.home', 'app.home', 'app.translations', 'anxApp.stories', 'anxApp.events', 'app.trade', 'anxApp.markets', 'anxApp.reports', 'anxApp.info', 'anxApp.ticker', 'inline']);

angular.module('app').controller('AppController', ["$scope", "$controller", "$translate", function($scope, $controller, $translate) {
  $controller('ApplicationController', {
    $scope: $scope
  });
  this.translation = function(key) {
    return $translate.instant(key);
  };
  this.translationEmpty = function(key) {
    var result;
    result = this.translation(key);
    if (typeof result === 'string' && result === '') {
      return true;
    }
    return true;
  };
  return this;
}]);

angular.module('app.home', ['ngRoute', 'templates']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/', {
    templateUrl: 'home/home.html'
  }).when('/home/announcements', {
    templateUrl: 'home/home.html'
  }).when('/home/market', {
    templateUrl: 'home/home.html'
  }).when('/home/press', {
    templateUrl: 'home/home.html'
  });
}]);

angular.module('anxApp.home').controller('HomeSliderController', ["$scope", function($scope) {
  var getIndexes, that;
  that = this;
  this.slides = eval('(' + $scope.slides + ')');
  getIndexes = function() {
    var k, result;
    result = [];
    for (k in that.slides) {
      result.push(k);
    }
    return result;
  };
  this.indexes = getIndexes();
  return this;
}]);

angular.module('anxApp.home').directive('homeSlider', ["$templateCache", function($templateCache) {
  return {
    restrict: "E",
    replace: true,
    template: $templateCache.get('home/home_slider.html'),
    scope: {
      slides: "@slides"
    },
    controller: 'HomeSliderController',
    controllerAs: 'slider'
  };
}]);

/**
 * Created by albert on 8/7/14.
 */

angular.module('anxApp.pages').config(['$translateProvider', function ($translateProvider) {

  $translateProvider.translations('en_US', {
    'HEAD_INVESTORS':     'Investor Information (Regulatory Compliance)',
    'INVESTORS_TEXT1_1':  '<p>DigaTrade Ltd is owned and operated by Bit-X Financial Corp.  Bit-X Financial Corp is a publicly traded company listed on the OTC.QB under the ticker symbol BITXF.  Bit-X Financial Corp is a reporting issuer in Canada with the British Columbia Securities Commission and in the United States with the Securities Exchange Commission “SEC”</p>' +
                          '<p>For more information on Bit-X Financial Corp (DBA - DigaTrade Ltd) please follow below links.</p>' +
                          '<h5>OTC MARKETS:</h5>' +
                          '<p><a href="http://www.otcmarkets.com/stock/BITXF/quote">http://www.otcmarkets.com/stock/BITXF/quote</a></p>' +
                          '<h5>SEC:</h5>' +
                          '<p><a href="http://www.sec.gov/cgi-bin/browse-edgar?company=Bit-X+Financial+Corp&owner=exclude&action=getcompany">http://www.sec.gov/cgi-bin/browse-edgar?company=Bit-X+Financial+Corp&owner=exclude&action=getcompany</a></p>' +
                          '<h5>SEDAR:</h5>' +
                          '<p><a href="http://www.sedar.com/DisplayProfile.do?lang=EN&issuerType=03&issuerNo=00027960">http://www.sedar.com/DisplayProfile.do?lang=EN&issuerType=03&issuerNo=00027960</a></p>' +
                          '<h5>Disclaimer</h5>' +
                          '<p>The information contained in this website is to provide a general overview of Bit-X Financial Corp (DBA -DigaTrade Ltd) activities and is not intended to be a comprehensive review of all matters concerning the company.  Bit-X Financial Corp or DigaTrade makes no representation nor does it provide any warranty as to the accuracy, currency or completeness of such information. The information contained in this website may be modified by Bit-X Financial Corp or DigaTrade at any time without prior notice. In no event shall Bit-X Financial Corp or DigaTrade be liable for any direct, indirect, consequential or other damages arising or connected with the use of this website or the information herein.  Bit-X Financial Corp (DBA - DigaTrade Ltd) is registered as a reporting company under section 12(b) of the Securities Exchange Act of 1934.</p>' +
                          '<h5>Forward-Looking Statements</h5>' +
                          '<p>This website may contain certain “Forward-Looking Statements” within the meaning of Section 21E of the United States Securities Exchange Act of 1934, as amended. All statements, other than statements of historical fact, included herein or in websites or documents linked hereto or otherwise publicly disclosed are forward-looking statements that involve various risks and uncertainties. There can be no assurance that such statements will prove to be accurate and actual results and future events could differ materially from those anticipated in such statements.</p>',
      'DISCLAIMER':       'DISCLAIMER: Bitcoin and crypto currencies prices are volatile and fluctuate day-to-day. Trading in these crypto currencies may be considered a high risk activity. Proper and sound judgement should be used in evaluating the risks associated with these activities. BITXF does not solicit nor make any representation that crypto currencies are an investment vehicle. The decision to trade crypto currencies rests entirely on the users own independent judgement.',
      'ABOUT-US':         'About Us',
      'TEAM':             'Team Profile',
      'PRIVACY':          'Privacy Policy',
      'SOCIAL':           'Social',
      'HELP':             'Help',
      'FAQ':              'FAQ',
      'API':              'API Reference',
      'TERMS':            'Terms of Use',
      'CONTACT':          'Contact Us',
      'CONTACTS':         'Contacts',
      'PARTNER':          'Partnership',
      'JOBS':             'Jobs',
      'SITE_NAME':        'DigaTrade',
      'HEAD_ABOUT':       'About Us',
      'HEAD_ADV':         'The ANX Advantage',
      'HEAD_BUYSELL':     'Buy/Sell Bitcoins',
      'HEAD_CAREERS':     'Careers',
      'HEAD_CARD':        'DigaTrade Bitcoin Debit Card',
      'HEAD_CRYPTO':      'Buy/Sell Altcoins',
      'HEAD_DISCLAIM':    'Disclaimer',
      'HEAD_FEES':        'Fees',
      'HEAD_KYC':         'KYC & AML',
      'HEAD_PRESS':       'In the Press',
      'HEAD_PRIVACY':     'Privacy Policy',
      'HEAD_VAULT':       'Bitcoins on the Go',
      'HEAD_SECURITY':    'Security',
      'HEAD_SERVICES':    'Services',
      'HEAD_START':       'Getting Started',
      'HEAD_STORE':       'ANX Retail Store',
      'HEAD_TERMS':       'Terms of Use',
      'HEAD_TANDC':       'Terms & Conditions',
      'FOOTER_VAULT':     'Mobile Wallet',
      'BUTTON_LOGIN':     "Log In",
      'BUTTON_REGISTER':  "Register",
      'BUTTON_SIGNUP':    "Sign Up for Free",
      'ABOUT_MORE_1':     "Find out more about",
      'ABOUT_MORE_2':     "The ANX Advantage",
      'ABOUT_TEXT1_1':
          '<p><b>DigaTrade<b/> is a Bitcoin Exchange, Multi-Currency Exchange and Full-Service Internet Financial Services Company owned and operated by Bit-X Financial Corp, a company located in Vancouver, British Columbia, Canada.  DigaTrade has executed an Exclusive Bitcoin Exchange and Services Agreement with Hong Kong based ANX for the North American market. The proprietary ANXPRO trading and matching engine manages high volume, high throughput, and low latency trading and was modeled on the same technology recently leveraged by the world’s largest Investment Banks. It also features blended multi-currency settlement in addition to real time FX pricing and risk management.</p>' +
          '<p>DigaTrade offers an easy, secure, and affordable platform to buy and sell the Bitcoin crypto-currency. DigaTrade offers a 24 hour online exchange that provides the automated matching of orders between its registered members and it strives to be your Bitcoin connection by making the experience as effortless as possible.</p>' +
          '<p>DigaTrade was established in Feb 2015 and is legally registered and based in British Columbia Canada.   DigaTrade has partnered with ANXPRO who have grown to be one of the most used Bitcoin exchange platforms and it is now one of the top 6 largest Bitcoin exchanges in the world by volume.</p>' +
          "<p>It is the DigaTrade’s mission to promote a healthy eco-system by providing value-added Bitcoin exchange services to the public. DigaTrade is also lowering the barriers to Bitcoins and other crypto currencies adoption by increasing ways for consumers to acquire and access crypto currencies.  DigaTrade has plans to introduce DigaTrade TELLER; a physical location at the Company headquarters and a globally usable multi-currency Bitcoin Debit Card.</p>" +
          '<h5 class="branded">Easy</h5>' +
          "<p>The DigaTrade user interface is focused on simplicity yet provides its' users with all the detail they expect when buying and selling Bitcoins. Our designers regularly hold focus groups with the public to obtain feedback on their user experience and preferred user interface. Ease of use and simplicity are the number one priority. If you're looking for confusing menus or complicated navigation then you've come to the wrong place!</p>" +
          '<h5 class="branded">Secure</h5>' +
          "<p>Security is the cornerstone of DigaTrade and the ANXPRO team. Every element of our operation has been methodically designed for optimum security. This includes all factors including physical intrusion, exhaustive vetting and background checking of staff members, the cold storage of coins and manual (yet efficient) processing of all withdrawal requests, and dedicated awareness of recurring security threats such as social engineering, phishing, and remote zero day exploits to name only a few. If you would like to know more about our security provisions, please review the Security section.</p>" +
          '<h5 class="branded">Affordable</h5>' +
          "<p>DigaTrade strives to provide the lowest fees in the Bitcoin market.</p>" +
          "<p>This results in improved liquidity and tighter spreads; and most importantly, increases your potential profits. We regularly host promotions for new and existing users.</p>" +
          '<h5 class="branded">THE DIGATRADE / ANX TRADING PLATFORM</h5>' +
          "<p>The proprietary DigaTrade / ANX trading and matching engine has been pioneered from the ground up, leveraging the skills of experienced developers with respected and long standing careers working for low latency software development firms. It is designed to manage high volume, high throughput, and low latency trading and was modeled on the same LMAX pattern now also leveraged by the world’s largest Investment Banks.</p>" +
          "<p>This investment banking grade trading platform has a simple and user friendly UI for users to buy and sell all major crypto currencies. It also features one consolidated shared order book for blended multi-currency settlement in addition to real time FX pricing and risk management.</p>" +
          "<p>The order engine delivers pre-scan indicative pricing and users can choose to either fix the quantity of Bitcoins or fix the price paid for every order. Lock in a guaranteed execution or alternatively lock in the ultimate price you're prepared to pay for your order; the choice remains yours. And this all relies on an order engine that achieves low latency performance along with the reliability of an exchange that has been verified in supporting millions of daily transactions. You can be confident knowing DigaTrade will dependably process orders in a fast moving market with both speed and efficiency.</p>",
      'ABOUT_TEXT2_H1':   "Management",
      'ABOUT_TEXT2_1':
          '<p><b>Brad Moynes, President Director</b></p>' +
          '<p>Brad Moynes is the President of DigaTrade and has developed the corporate structure and business model to meet the company vision of launching a crypto-currency bitcoin exchange.  He brings to the Company 18 years of public finance and corporate management experience and currently serves on the board of several publically traded micro-cap companies. Brad is a member of the Canadian Securities Institute 1997 & 2010, a previous Investment Advisor with and maintains his Personal Information Form “PIF” with the British Columbia Securities Commission “BCSC” and the Securities Exchange Commission “SEC”.  Brad has become an experienced bitcoin and altcoin trader and has invested in a bitcoin exchange, bitcoin mining, altcoin investments; including Bitcoin, Ripple, Steller, Peercoin, Litecoin, Dogecoin and a Peer to Peer lending site among many other bitcoin and crypto-currency related ventures over the past several years.</p>',
      'ABOUT_TEXT2_2':
          '<p><b>Peter Jensen, BSc, LLB, Corporate Secretary &amp; Counsel</b></p>' +
          '<p>Peter Jensen serves as the company’s corporate and legal advisor and is a member of the Beadle Raven Securities Law Corporation. Jensen has broad international legal counseling experience and a depth of knowledge in trans-border transactions for Canadian and US companies wishing to become US or Canadian reporting and trading issuers. He has extensive experience in conducting legal affairs for clients in the Orient, Europe, Africa and Latin America and has served as a director of companies with capitalization in the hundreds of millions of dollars.</p>',
      'ABOUT_TEXT2_3':
          '<p><b>Paul Heney, BSocSc, LLB, MBA, Director</b></p>' +
          '<p>Paul Heney was a founding partner of Ubequity Capital and has acted as legal advisor and business partner to numerous successful industrialists, entrepreneurs and venture capitalists in Canada and internationally. He has practiced securities and corporate law, focusing on public and private securities offerings serving the venture capital/small cap market, as well as on mergers and acquisitions and securities regulatory matters for issuers and investment bankers. Paul helped directly in the success of one of Ubequity’s major projects, Silverback Media PLC., completing six acquisitions and raising several million dollars of equity and debt financing. Silverback has grown to become one of the world’s largest, independent mobile marketing and advertising agencies, with operations in Canada, the U.S., France and the UK.</p>',
      'ADV_SUBHEAD':      "reasons why we should be your Bitcoin connection",
      'ADV_H1_1':         "Safe & Secure",
      'ADV_H2_1':         "Legal & Trustworthy",
      'ADV_H3_1':         "High Liquidity",
      'ADV_H4_1':         "Customer Support",
      'ADV_H5_1':         "No Fees",
      'ADV_H6_1':         "Immediate Processing",
      'ADV_H7_1':         "Mulitple Payment Methods",
      'ADV_H8_1':         "Multi-currencies Support",
      'ADV_1_1':          "<p>Security is the cornerstone of ANX. Here are just a a few factors we use to give you a peace of mind<ul>" +
          "<li>Supports 3-Factor Authentication</li>" +
          "<li>Cold storage is utilised to prevent against theft or loss</li>" +
          "<li>Segregated customer funds and do not operate a fractional reserve</li>" +
          "<li>Partnered with the world’s leading DDOS Service and Hosting Providers</li>" +
          "<li>Multi-firewall protection</li>" +
          "<li>Servers are hosted in a Tier 3+ ISO 27001/9001 compliant data centre</li></ul>" +
          "<p>If you would like to know more about our security provisions, please click here.</p>",
      'ADV_2_1':          "<p>ANXBTC.COM is committed in providing a safe, globally-compliant and reputatable service to you. We pride ourselves on the integrity and transparency of our business. We are:" +
          "<ul><li>A globally recognized brand and top 6 Bitcoin exchange in the world</li>" +
          "<li>Formally registered company in Hong Kong since June 2013 and a fully licensed Money Services Operator (MSO), the first MSO licensed exchange in Asia</li>" +
          "<li>The founding partners have financial markets, management consulting, banking technology and compliance backgrounds. They are well and regulations, especially KYC and AML obligations.</li></ul></p>",
      'ADV_3_1':          "<p>We constantly strive to improve liquidity and tighten our spreads to provide the best price in the Bitcoin market ensuring your potential profits is maximized." +
          "<ul><li>Top 6 largest Bitcoin exchanges in the world by volume</li>" +
          "<li>The only global platform that is able to process trades from the two largest Bitcoin markets: USA and China</li>" +
          "<li>Tight spreads and high liquidity</li></ul></p>",
      'ADV_4_1':          "<p>Our customer support teams at ANX are trained in all aspects of operations and security and most importantly remain Bitcoin loyalists. We offer:" +
          "<ul><li>Multilingual support (English, Traditional Chinese, Simplified Chinese, Portugese)</li>" +
          "<li>Our customer support Key Performance Indicator is to respond to and resolve all inquiries within 12 hours of inquiry</li></ul>",
      'ADV_5_1':          "<p>You are not charged transaction fees when using ANXBTC to buy and sell bitcoins.The price displayed is the total price and your account will be credited and debited as per the details provided on screen." +
          "<ul><li>No trading fees</li>" +
          "<li>No bank transfer deposit fees</li>" +
          "<li>No Bitcoin transfer fees</li>" +
          "<li>No additional or hidden fees</li></ul>" +
          "<p>Please see fee schedule for details.</p>",
      'ADV_6_1':          "<p>Our support team promptly check all requests to make sure we faciliate your account effectively and efficiently" +
          "<ul><li>Immediate processing of crypto currency deposits and withdrawals</li>" +
          "<li>Real time processing of fiat deposits and withdrawals</li></ul>",
      'ADV_7_1':          "<p>We provide multiple payment methods for you to efficiently fund your account.<br>" +
          "We have <strong>local banking presence in Hong Kong, China and Australia</strong>.<br>" +
          "Simply fund your account via any of the following methods:</p>",
      'ADV_7_2':          "ATM Cash Deposit",
      'ADV_7_3':          "SEPA",
      'ADV_7_4':          "In-store Cash Deposit",
      'ADV_7_5':          "International Bank Wire",
      'ADV_7_6':          "Local Bank Transfer",
      'ADV_7_7':          "ZipZap",
      'ADV_8_1':          "<p>We support buying and selling Bitcoins in all 10 global fiat currencies:</p>" +
          "",
      'ADV_9_1':          "Find out more about",
      'ADV_9_2':          "Our Security",
      'BTN_COMPARE':      "Compare Cards <i class='fa fa-credit-card'></i>",
      'BTN_GETONE':       "Get One Now",
      'BUYSELL_SUBHEAD':  "buying or selling bitcoins has never been easier",
      'BUYSELL_TEXT1_H1': "Instant Buy Instant Sell",
      'BUYSELL_TEXT1_1':  "Provides you with a no fuss approach to manage your bitcoins! You are no longer required to understand bids, asks, or exotic order types in order to buy and sell bitcoins",
      'BUYSELL_TEXT2_H1': "Buy Bitcoins",
      'BUYSELL_TEXT2_1':  "Simply fund your ANX account through any of the supported deposit methods and using the instant-buy feature, enter the amount of bitcoin you wish to purchase and click the Place Order button. The total price debited from your account will be displayed for 30 seconds for you to confirm your purchase. If you do not confirm your purchase within 30 seconds, a new updated price will be displayed. Once confirmed, bitcoins are instantly delivered to your ANX wallet. Your bitcoins can be immediately withdrawn without delay should you wish to store your bitcoins on an alternative wallet",
      'BUYSELL_TEXT2_H2': "Sell Bitcoins",
      'BUYSELL_TEXT2_2':  "To sell your bitcoins, transfer them to your ANX bitcoin wallet and use the instant-sell feature to enter the amount of bitcoin you wish to sell and then click the Place Order button. The total price credited to your account for the sale of your bitcoins will be displayed; the amount is displayed for 30 seconds for you to confirm your sell request. Once confirmed, funds are instantly credited to your ANX account. You can immediately withdraw your funds without delay, subject to minimum withdrawal limits.",
      'BUYSELL_TEXT2_H3': "Competitive Price",
      'BUYSELL_TEXT2_3':  "To ensure the most competitive pricing for you, ANX has introduced the ANX Dealer Network, exclusively developed for the ANXBTC platform. When a buy or sell order is submitted, the ANXBTC platform receives matching orders from an extensive number of buyers and sellers who have registered as part of the ANX Dealer Network. The ANXBTC platform instantly determines the best price and displays this on screen for 30 seconds each time you submit an order.",
      'BUYSELL_TEXT2_H4': "No Transaction Fees",
      'BUYSELL_TEXT2_4':  "You are no longer charged transaction fees when using the ANXBTC platform to buy and sell bitcoins. The price displayed for buying and selling bitcoins is determined by the members of the ANX Dealer Network who are actively participating at the time your order is submitted. The price displayed is the total price and your account will be credited and debited as per the details provided on screen. There are no additional or hidden fees",
      'BUYSELL_TEXT3_H1': "Immediate Processing",
      'BUYSELL_TEXT3_1':  "buy and sell orders are processed instantly",
      'BUYSELL_TEXT3_H2': "Safe & Secure",
      'BUYSELL_TEXT3_2':  "With the use of 3-Factor Authentication, we provide extra protection to prevent unauthorized access to your account. We hold our funds in cold storage to ensure your bitcoins are safely kept in your ANX wallet.",
      'BUYSELL_TEXT3_H3': "Multiple Payment Methods",
      'BUYSELL_TEXT3_3':  "Support for International Bank Wire, Local Bank Transfer, In-Store and ATM Cash Deposit",
      'BUYSELL_TEXT3_H4': "Multi-currencies & Bitcoin Trading",
      'BUYSELL_TEXT3_4':  "Fund your account in all major currencies: USD, EUR, HKD, AUD, CAD, JPY, SGD, GBP and NZD",
      'BUYSELL_TEXT4_1':  "or",
      'BUYSELL_TEXT4_2':  "to start trading now!",
      'BUYSELL_TEXT5_1':  "If you prefer to trade your bitcoins at pricing and times of your choice, try our acclaimed ANXPRO Exchange. With Algo support, Altcoins and much more-this is the one place to handle all your cryptocurrency needs! <em>Simply login with your existing ANXBTC account details, one account for both ANXBTC and ANXPRO!</em>",
      'CAREERS_TEXT1_1':  "<p>The Bitcoin currency is changing the world and it has already started. Be part of it.</p><p>We're always eager to hear from the industry's most enthusiastic people. Introduce yourself and tell us more about what you can offer by emailing to <a href='mailto:careers@anxintl.com'>careers@anxintl.com</a></p>",
      'CAREERS_TEXT2_H1': "Current Vacancies",
      'CAREERS_TEXT2_1':  "Forum Manager | 2014-08-08 | Permanent<br>Front End Developer | 2014-08-08 | Permanent<br>UX Developer | 2014-08-08 | Permanent<br>",
      'CRYPTO_TEXT1_1':   "A full featured exchange platform that allows you to buy and sell bitcoins and other Altcoins. The platform is simple enough for beginners and sophistricated enough for professional traders. One single integrated account for both ANXBTC and ANXPRO.",
      'CRYPTO_TEXT2_H1':  "Peer to Peer Crypto-currencies Support",
      'CRYPTO_TEXT2_1':   "Bitcoin was introduced in January 2009 by an unknown person using the alias Satoshi Nakamoto.  It is the first decentralized peer-to-peer payment network that is powered by its users with no central authority or middlemen. From a user perspective, Bitcoin is pretty much like cash for the Internet.  For more information about Bitcoin, visit our FAQ section.",
      'CRYPTO_TEXT2_2':   "Litcoin was introduced in October 2011 and is nearly identical to bitcoin except that it has a higher total of coins (84 million) and processes a block every 2.5 minutes rather than bitcoin's 10 minute interval. it also uses scrypt in its proof-of-work algorithm so there is not as much of an advantage to miners using ASIC mining equipment.",
      'CRYPTO_TEXT2_3':   "Dogecoin was introduced in December 2013 and had a fast initial coin production schedule with approximately 100 billion coins in circulation by the end of 2014 and with an additional 5.2 billion coins every year thereafter. It has gained traction in the social space and as an internet tipping coin.",
      'CRYPTO_TEXT2_4':   "Namecoin was introduced in April 2011 and is recognized as the first fork of the Bitcoin software. Aside form being a crypto-currency, it also acts as an alternative decentralized DNS, which would avoid domain name censorship.",
      'CRYPTO_TEXT2_5':   "Peercoin was introduced in August 2012 and is known as a hybrid proof-of-stake/proof-of-work coin. Unlike other coins, there is no hard limit to the amount that can be mined and instead has a limited release rate plus 1% decentralized inflation built in.",
      'CRYPTO_TEXT2_6':   "Stellar was introduced in August 2014 and it is part of a nonprofit organization, the Stellar Development Foundation, to encourage the wider adoption of digital currencies through the offering of a decentralized gateway for digital currency-to-fiat currency transfers, as well as featuring itself as an internal digital currency of its own.",
      'CRYPTO_TEXT2_7':   "Ripple was first introduced in 2004 by Ryan Fugger and later further developed by Jed McCaleb in 2011. It is the name of both a digital currency (XRP) and an open payment network within which that currency is transferred. It is a distributed, open-source payments system that is still in its beta. As of 2014, Ripple is the second largest cyrptocurrency by market capitalization after Bitcoin and Litecoin.",
      'CARD_SUBHEAD':     "let your Bitcoins do everything your credit cards can do",
      'CARD_TEXT_Q':      'Questions? Contact us at <a href="mailto:cards@anxintl.com">cards@anxintl.com</a>',
      'CARD_TEXT1_0':     "* Card design is for demonstration only. The actual debit card design will be different.",
      'CARD_TEXT1_H1':    "Transfer your Bitcoins directly to your ANX debit card anytime anywhere!",
      'CARD_TEXT1_1':     "Automatically links to your ANX account so that you can conveniently fund your debit card for everyday use.",
      'CARD_TEXT2_H1':    "Spend it anywhere in the world just like a credit card",
      'CARD_TEXT2_H2':    "Global ATM Network",
      'CARD_TEXT2_2':     "Withdraw cash anytime anywhere you like. The card can be used at over 25 Million locations worldwide",
      'CARD_TEXT2_H3':    "Point of Sale Purchases",
      'CARD_TEXT2_3':     "Make POS purchases at any brick and mortar stores in every corner of the world!",
      'CARD_TEXT2_H4':    "Online Shopping Worldwide",
      'CARD_TEXT2_4':     "Use it for online transactions and purchases around the world.",
      'CARD_TEXT2_H5':    "Say no to paperwork, application process and credit check!",
      'CARD_TEXT3_1':     "<a href=\"/\">Login</a> to get your ANX Bitcoin Debit Card today, it's easy, secure and affordable!",
      'CARD_TEXT3_2':     "<a href=\"/\">Login</a> or <a href=\"/register\">Sign up</a> to get your ANX Bitcoin Debit Card today!",
      'CARD_TEXT4_H1':    "SAFER THAN CARRYING CASH",
      'CARD_TEXT4_1':     "If your card is lost or stolen, you can put a block on it and recover your money. Something you can’t do with cash!",
      'CARD_TEXT4_H2':    "MORE CONVENIENT THAN CASH",
      'CARD_TEXT4_2':     "No more fumbling with cash when making purchases and say no to exchanging money when going abroad, just use your card!",
      'CARD_TEXT4_H3':    "LIMITLESS FLEXIBILITY",
      'CARD_TEXT4_3':     "Get your choice of currency and enjoy benefits at participating merchants.",
      'CARD_TEXT4_H4':    "ACCEPTED WORLDWIDE",
      'CARD_TEXT4_4':     "ANX Bitcoin Debit Cards are part of a global payment network with over 25 million merchants.  You can make purchase at any of these merchants or withdraw cash on their ATM network worldwide.",
      'CARD_TEXT4_H5':    "MANAGE YOUR MONEY ANYWHERE",
      'CARD_TEXT4_5':     "With your ANX account, you can check your card balance and account information on the web or through your mobile phone.",
      'CARD_TEXT4_H6':    "EASY TO GET",
      'CARD_TEXT4_6':     "Getting an ANX Bitcoin Debit Card is extremely easy! There is no paperwork, no troublesome application process and no credit check required.",
      'CARD_TEXT5_H1':    "FEATURED CARDS",
      'CARD_TEXT5_1':     "With ANX Bitcoin Debit Cards, you can enjoy an exciting world of offers, benefits and convenience. ANX offers various debit card options to make sure you get the card that is best suited for your everyday use.",
      'CARD_TEXT5_H2':    "ANX Basic",
      'CARD_TEXT5_2':     "<ul><li>Based in USD</li>" +
          "<li>Online purchasing</li>" +
          "<li>Online account management</li>" +
          "<li>No monthly fee</li></ul>",
      'CARD_TEXT5_H3':    "ANX Premium",
      'CARD_TEXT5_3':     "<ul><li><strong>No loading fees</strong></li>" +
          "<li>Based in USD</li>" +
          "<li>Free POS purchases</li>" +
          "<li>Speed loading</li></ul>",
      'CARD_TEXT5_4':     'We offer special pricing on bulk orders (minimum of 10 cards), for more details please contact <a href="mailto:cards@anxintl.com">cards@anxintl.com</a>',
      'CARD_TEXT6_H1':    "How it Works",
      'CARD_TEXT7_H1':    "Get A Card",
      'CARD_TEXT7_1':     "Login or sign up to order card online.",
      'CARD_TEXT7_H2':    "Load",
      'CARD_TEXT7_2':     "Transfer Bitcoins to add funds to your card.",
      'CARD_TEXT7_H3':    "Use",
      'CARD_TEXT7_3':     "Use your card to get cash or make purchases at anywhere anytime.",
      'CARD_TEXT7_H4':    "Reload",
      'CARD_TEXT7_4':     "Add more funds to your card via our online portal.",
      'CARD_TABLE1_H1':   "Features",
      'CARD_TABLE1_H2':   "ANX Basic",
      'CARD_TABLE1_H3':   "ANX Premium",
      'CARD_TABLE1_H4':   "ANX Plus",
      'CARD_TABLE1_H5':   "Card Parameters",
      'CARD_TABLE1_H6':   "Fees & Charges",
      'CARD_TABLE1_H7':   "Help & Support",
      'CARD_TABLE1_H8':   "Rewards & Offers",
      'CARD_TABLE1_R1_1': "Card Currency",
      'CARD_TABLE1_R1_2': "USD",
      'CARD_TABLE1_R1_3': "USD",
      'CARD_TABLE1_R1_4': "HKD",
      'CARD_TABLE1_R2_1': "No Credit Check",
      'CARD_TABLE1_R3_1': "ATM Withdrawals",
      'CARD_TABLE1_R4_1': "POS Purchases",
      'CARD_TABLE1_R5_1': "Online Purchases",
      'CARD_TABLE1_R6_1': "Mobile App",
      'CARD_TABLE1_R6_3': "Coming Soon",
      'CARD_TABLE1_R7_1': "Online Account Management",
      'CARD_TABLE1_R7_3': "Coming Soon",
      'CARD_TABLE1_R7_4': "Coming Soon",
      'CARD_TABLE1_R8_1': "Speed Loading",
      'CARD_TABLE1_R8A_1': "International Merchant Network",
      'CARD_TABLE1_R8B_1': "Card Reloadable",
      'CARD_TABLE1_R9_1': "Maximum Card Balance",
      'CARD_TABLE1_R10_1':"Maximum Amount per card load",
      'CARD_TABLE1_R11_1':"ATM Withdrawals",
      'CARD_TABLE1_R11_3':"No Limit, Subject to limits on local ATM",
      'CARD_TABLE1_R11_4':"US$1000, Subject to limits on local ATM",
      'CARD_TABLE1_R12_1':"POS Purchases",
      'CARD_TABLE1_R13_1':"Online Transactions",
      'CARD_TABLE1_R14_1':"ATM Withdrawals",
      'CARD_TABLE1_R14_4':"1%, Min. US$3",
      'CARD_TABLE1_R15_1':"POS Purchases",
      'CARD_TABLE1_R16_1':"ATM Balance Inquiry",
      'CARD_TABLE1_R16_3':"Free",
      'CARD_TABLE1_R17_1':"Monthly Fee",
      'CARD_TABLE1_R17_2':"Free",
      'CARD_TABLE1_R17_3':"Free",
      'CARD_TABLE1_R17_4':"Free for first 12 months<br>HK$5/month thereafter<br>deducted from card balance",
      'CARD_TABLE1_R18_1':"Card Load Fee",
      'CARD_TABLE1_R18_2':"US$5 + 2.5% of recharge amount",
      'CARD_TABLE1_R18_3':"Free",
      'CARD_TABLE1_R18_4':"Free",
      'CARD_TABLE1_R19_1':"FAQs",
      'CARD_TABLE1_R20_1':"Telephone",
      'CARD_TABLE1_R21_1':"Email",
      'CARD_TABLE1_R22_1':"Welcome Offers",
      'CARD_TABLE1_R22_2':"Get a refund on your shipping fee when you charge the required amount on your first loading",
      'CARD_TABLE1_R22_3':"load HK$8,000",
      'CARD_TABLE1_R22_4':"load US$5,000",
      'CARD_TABLE1_R23_1':"Refer a Friend",
      'CARD_TABLE1_R23_2':"Get cash bonus when you refer a friend",
      'CARD_TABLE1_R24_1':"One for One",
      'CARD_TABLE1_R24_2':"Enjoy FREE upgrade to ANX Plus or ANX Premium if you currently own a ANX Basic card",
      'CARD_TABLE1_R25_1':"Merchant Offers",
      'CARD_TABLE1_R25_2':"Enjoy exclusive savings at participating merchants",
      'CARD_TABLE1_R26_1':"Holiday Offers",
      'CARD_TABLE1_R26_2':"Get a FREE ANX Plus card when you verify your account by January 31 2015",
      'CARD_TABLE1_R27_1':"Get a FREE upgrade to ANX Premium when you choose express delivery for receiving verification code",
      'CARD_TABLE1_R28_1':"Order a new ANX Premium card and load 0.9BTC on the first loading to get the card for FREE",
      'CARD_TC_TEXT_H1':  'ANX Basic',
      'CARD_TC_TEXT_H2':  'ANX Plus',
      'CARD_TC_TEXT_H3':  'ANX Premium',
      'CARD_TC_TEXT_1':   '<p>Terms and Conditions for Foreign Currency Prepaid Bearer Card, distributed by ANX as an electronic money instrument</p>' +
          '<p>I. Introductory provisions and definitions</p>' +
          '<p>These Terms and Conditions for Foreign Currency Prepaid Bearer Card, distributed by ANX as an electronic money instrument (“Terms and Conditions”) shall regulate the rules for issuance settlement of Transactions concluded with the Card.</p>' +
          '<p>As used in these Terms and Conditions, the following terms and abbreviations shall mean:</p>' +
          '1. ANX Support Team - The dedicated personnel at ANX whom provide services associated with the issuing and management of the ANX debit card,<br>' +
          '2. Merchant – an entrepreneur who accepts payment for the offered goods or services in non-cash form, with the use of the Card,<br>' +
          '3. Authorisation – the Cardholder’s consent to a Transaction, expressed as set out in these Terms and Conditions,' +
          '4. Bank – The underlying bank associated with the card issuance,' +
          '5. ATM – a device enabling the Cardholder to withdraw cash using the Card, thus withdrawing electronic money from the Card, or to perform other Transactions,' +
          '6. Card Block – temporary blockade imposed by the Bank to prevent the making of Transactions, leading to the blockade of the Available Funds Balance on the Card,' +
          '7. CardMailer – a letter to which the Card is attached at the time of its distribution by ANX,' +
          '8. Business Day – a day other than Saturday or Sunday or a public holiday,' +
          '9. Card – the bearer card, distributed as an electronic money instrument, with the possibility of Card Top-Up,' +
          '10. code / CVV2 code – a three-digit code of the payment organisation placed on the back of the card that is used to authenticate the Card when making Internet, telephone and mail transactions,' +
          '11. PIN – a confidential identification number provided to the Cardholder with the Card, which together with data recorded on the Card is used for electronic identification of the Cardholder,' +
          '12. Transaction Limits – amount or quantity limits set for the individual Transaction types, up to which the Bank authorises the Transactions,' +
          '13. Payment Organisation – the Visa Europe organisation of financial institutions, within which the Bank, on the basis of license held, issues the Cards.' +
          '14. Bank Outlet – corporate branch, corporate office or other outlet of the Bank,' +
          '15. Cardholder – each time the bearer of the Card, who acquired the Card and the CardMailer in a manner permitted under these Terms and Conditions and the generally applicable laws and regulations, thus becoming a party to the Agreement,' +
          '16. Card Account – an account maintained in the Card Account Currency,' +
          '17. Available Funds Balance – the amount of funds stored in the Card Account, up to which Transactions can be made,' +
          '18. POS Terminal – an electronic payment terminal that enables the Cardholder to make Transactions, including a POS Terminal equipped with a contactless reader, enabling contactless transactions by placing the Card in close proximity of the reader,' +
          '19. Transaction – a payment made with the Card or cash withdrawal with the use of the Card, especially from an ATM,' +
          '20. Partial Transaction – payment made with the Card up to the amount of the Available Funds Balance (where the Transaction amount is higher than the Available Funds Balance on the Card) and allowing the payment of the remaining Transaction amount in another manner, including with another payment instrument or with cash,' +
          '21. Contactless Transaction – a non-cash transaction made via a POS Terminal enabling payments by placing the card in the close proximity of the contactless reader,' +
          '22. Remote transaction – a non-cash payment Transaction made without the physical presentation of the Card (over the phone, via the Internet, via mail or by fax),' +
          '23. Agreement – the agreement on the electronic money instrument, concluded at the time of the first Card Top-Up and in force up to the expiry date of the Card. The Agreement’s validity period may not be extended,' +
          '24. Card Account Currency – the currency printed on the CardMailer, in which the Card Account is maintained and the Available Funds Balance is stored. ANX currently distributes the Cards in the following currencies: USD' +
          '25. Card’s Settlement Currency – a currency that is used by the Payment Organisation to settle Transactions made with the Cards, in that case the Card Account Currency,' +
          '26. Buy-Out of Electronic Money – a demand submitted by the Cardholder, which results in the refund of funds kept in the Card Account, under rules set forth in these Terms and Conditions,' +
          '27. Card Top-Up – increasing the Available Funds Balance by transferring funds into the Card Account,' +
          '28. Card Freeze – a situation where the Card is permanently blocked in the Bank’s system and with the Payment Organisation, made at the request of the Cardholder or under the Bank’s and ANX’s decision.',
      'CARD_TC_TEXT_2':   '',
      'CARD_TC_TEXT_3':   '',
      'CONTACT_TEXT_H1':  'Customer Service',
      'CONTACT_TEXT_1':   '<p>Our customer support teams at ANX are trained in all aspects of operations and security and most importantly remain Bitcoin loyalists.</p>' +
          '<p>We are at your service and ready to answer any questions you may have. Our customer support Key Performance Indicator is to respond to and resolve all inquiries within 12 hours of inquiry however in most cases the response time is significantly less. Our commitment to customer service is unmatched and embedded into the DNA of ANX staff. Your feedback is very important to us and we regularly implement new features requested by ANX users.</p>' +
          '<p>Support Contact: <a href="mailto:support@anxbtc.com">support@anxbtc.com</a></p>',
      'CONTACT_TEXT_H2':  'Social Channels',
      'CONTACT_TEXT_H3':  'Headquarters',
      'CONTACT_TEXT_3':   'ANX INTERNATIONAL',
      'CONTACT_TEXT_4':   "Unit 701, 7/F, The Sun's Group Centre,<br>200 Gloucester Road, Wanchai Hong Kong<br>Monday to Friday, 9am to 6pm",
      'DISCLAIM_TEXT_1':  "<p>It is your responsibility to determine what, if any, taxes apply to the payments you make or receive, and it is your responsibility to collect, report an remit the correct tax to the appropriate tax authority, regardless of jurisdiction. DigaTrade Ltd is not responsible for determining whether taxes apply to your transaction, or for collecting, reporting or remitting any taxes arising from any transaction, to you or any taxation, governing or third authority.</p>" +
          "<p>Trading Bitcoins, virtual currencies and virtual commodities carries a high level of risk, and may not be suitable for everyone. Before deciding to buy or sell these instruments you should carefully consider your investment objectives, level of experience, and risk appetite. The possibility exists that you could sustain a loss of some of all of your investment and therefore you should not invest money that you cannot afford to lose. You should be aware of all the risks associated with virtual commodities, and seek advice from an independent financial adviser should you have any doubts. Furthermore, there may be risks that are not disclosed in our Terms of use. You should use extreme consideration and be conscious of assessing your financial situation and tolerance for risk before engaging in activities involving the trading of virtual commodities.</p>" +
          "<p>Any opinions, news, research, analyses, prices, or other information contained on this website is provided as general market commentary, and does not constitute investment advice. DigaTrade will not accept liability for any loss or damage, including without limitation to, any loss of profit, which may arise directly or indirectly fro use of or reliance on such information.</p>" +
          "<p>There are risks associated with utilizing an Internet-based deal execution trading system including, but not limited to, the failure of hardware, software, and Internet connection. As DigaTrade does not control the reliability or availability of the Internet, DigaTrade Ltd cannot be responsible for communication failures, distortions or delays when trading via the Internet.</p>" +
          "<p>The content on this website is subject to change at any time without notice, DigaTrade has taken reasonable measures to ensure the accuracy of the information on the website, however, does not guarantee its accuracy, and will not accept liability for any loss or damage which may arise directly or indirectly from the content or your inability to access the website, for any delay in or failure of the transmission or the receipt of any instruction or notifications sent through website.</p>" +
          "<p>In case of discrepancy between the English version and other language versions in respect of all or any part of the contents in the website, the English version shall prevail.</p>",
      // Override default fees table
      'FEES_TEXT_1':      '<table class="table table-condensed">' +
                          "   <thead>" +
                          "       <tr>" +
                          "           <th>Funding Type</th>" +
                          "           <th>Fee</th>" +
                          "       </tr>" +
                          "   </thead>" +
                          "   <tbody>" +
                          "       <tr><td>All Fiat Currencies Deposit</td><td>Free</td></tr>" +
                          "       <tr><td>Deposit BTC</td><td>Free</td></tr>" +
                          "       <tr><td>Withdraw via HK Local Bank Transfer (CAD)</td><td>CAD 5 + 0.5% of withdrawal amount</td></tr>" +
                          "       <tr><td>Withdraw via Bank Wire (CAD)</td><td>CAD 20 + 0.5% of withdrawal amount</td></tr>" +
                          "       <tr><td>Withdraw BTC</td><td>Free (Exc. Crypto Network Fee)</td></tr>" +
                          "   </tbody>" +
                          "</table>" +
                          "<p>In some cases your bank or other third party intermediary may charge you additional fees upon wire-in or wire-out operations. These fees are at the discretion of your bank or the third party intermediary and are in no way associated with {{ brand }}’s fee schedule.</p>" +
                          "<h4>Trading Fee</h4>" +
                          "<p>You will be charged 0.6% on orders that are filled immediately (i.e. taker) regardless of whether they are submitted as limit or market order.</p>" +
                          "<p>You will be charged 0.3% on limit orders that are resting (i.e. maker) in the market prior to being filled.</p>",
      'FEES_TEXT_2':      '<table class="table table-condensed">' +
                          '   <thead>' +
                          '       <tr>' +
                          '           <th>Account Status</th>' +
                          '           <th>Minimum Fiat Currency Deposit</th>' +
                          '           <th>Maximum Fiat Currency Deposit</th>' +
                          '           <th>Minimum Fiat Currency Withdraw</th>' +
                          '           <th>Maximum Fiat Currency Withdraw</th>' +
                          '           <th>Deposit Cryptocurrency</th>' +
                          '           <th>Withdraw Cryptocurrency</th>' +
                          '       </tr>' +
                          '   </thead>' +
                          '   <tbody>' +
                          '       <tr><td>Unverified</td><td>Not Available</td><td>Not Available</td><td>Not Available</td><td>Not Available</td><td>No Limit</td><td>20 BTC</td></tr>' +
                          '       <tr><td rowspan="5">Verified</td><td rowspan="5">CAD 500 via Bank Wire</td><td rowspan="5">CAD 10,000,000 via Bank Wire</td><td rowspan="5">CAD 100 via Bank Wire<br>CAD 100 via<br>HK Local Bank Transfer</td><td rowspan="5">CAD 10,000 via Bank Wire<br>CAD 10,000 via<br>HK Local Bank Transfer</td><td rowspan="5"></td><td>100 BTC</td></tr>' +
                          '       <tr><td>750 LTC</td></tr>' +
                          '       <tr><td>50,000,000 DOGE</td></tr>' +
                          '       <tr><td>1,000,000 STR</td></tr>' +
                          '       <tr><td>1,000,000 XRP</td></tr>' +
                          '   </tbody>' +
                          "</table>",
      'FOOTER_ABOUT':     'DigaTrade',
      'KYC_TEXT_1':       "<p>KYC (Know-Your-Customer) and AML (Anti Money Laundering) Policies</p>" +
          "<p>DigaTrade Ltd remains committed in providing a safe, globally-compliant and reputable service to its clients. As a company, we pride ourselves on the integrity and transparency of our business. For this reason, DigaTrade relies on comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) policies.</p>" +
          "<p>DigaTrade Ltd is legally registered and its infrastructure is physically located in Vancouver, British Columbia, Canada. DigaTrade insists on a comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) compliance framework. This includes the monitoring of suspicious transactions and obligatory reporting to local regulators and other compliance bodies.</p>" +
          "<p>Our AML and KYC policies differ depending on the country of origin of which our clients are located and furthermore recorded against through the DigaTrade registration process. The specific AML and KYC policies as per regional jurisdiction are located below. Our robust compliance framework ensures that regulatory requirements are being adhered to at both a local and global level, instilling a level of trust and ensuring DigaTrade will continue to operate indefinitely.</p>" +
          "<p>DigaTrade Ltd reserves the right to refuse registration to persons from or in jurisdictions that do not meet international AML standards or could be considered as a Politically Exposed Person.</p>" +
          "<p>DigaTrade Ltd reserves the right to carry out customer due diligence to verify its users and their transactions. Enhanced customer due diligence will be carried out periodically as part of our ongoing risk review and assessment. In addition to this, any attempt to abuse DigaTrade or its platform will result in an immediate account suspension and reported to the respective authorities.</p>" +
          "<p>The DigaTrade AML and KYC policies are as follows:</p>" +
          "<ul>" +
          "<li>Transactions conducted via the DigaTrade Ltd platform will be subject to AML transaction monitoring.</li>" +
          "<li>Identity and proof of address documents and transaction records will be maintained for at least six years and will be submitted to regulators as required without prior notification to registered users.</li>" +
          "<li>DigaTrade may at any time without liability and without disclosing any reason, suspend the operation of your account. DigaTrade shall notify you after exercising its rights under this clause.</li>" +
          "<li>Registered members of whom transactions involve fiat currency will be required to verify their identity and adhere to the DigaTrade KYC policy. This includes the submission of both government ID and proof of address. Valid ID includes a passport, national card, or drivers license. Valid proof of address includes utility and rates bills not older than 3 months. Other forms of ID and Address verification will not be accepted. Your account will be unregistered until account verification has been completed.</li>" +
          "<li>Suspicious transactions will result in a Suspicious Activity Report being submitted to the Joint Financial Intelligence Unit ('JFIU').</li>" +
          "</ul>",
      'PARTNER_TEXT_1':   "<p>The founders of ANXBTC.COM are approachable in discussing potential opportunities with both institutions and individuals. Please direct initial inquiries to <a href=\"mailto:support@anxbtc.com\">support@anxbtc.com</a></p>",
      'PRIVACY_TEXT_1':   "<p><strong>What information do we collect?</strong></p>" +
          "<p>We collect information from you when you register on our site, place an order, subscribe to our newsletter or fill out a form. When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address or phone number. You may, however, visit our site anonymously.</p>" +
          "<p><strong>What do we use your information for?</strong></p>" +
          "<p>Any of the information we collect from you may be used in one of the following ways:</p>" +
          "<ul>" +
          "<li>To personalize your experience (your information helps us to better respond to your individual needs)</li>" +
          "<li>To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>" +
          "<li>To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</li>" +
          "<li>To process transactions (Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.)</li>" +
          "<li>To send periodic emails (The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.)</li>" +
          "<li>To meet regulatory and/or compliance obligations (we are required to verify your identity and obtain additional information when your account transacts in fiat currencies)</li>" +
          "</ul>" +
          "<p>How do we protect your information?</p>" +
          "<p>We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access your personal information. We offer the use of a secure server. All supplied sensitive information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be accessed only by those authorized with special access rights to our systems, and are required to keep the information confidential.</p>" +
          "<p>After a transaction, your private information will not be kept on file for more than 60 days.</p>" +
          "<p><strong>Do we use cookies?</strong></p>" +
          "<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information. We use cookies to help us remember as well as to process the items in your shopping cart and understand and save your preferences for future visits.</p>" +
          "<p><strong>Do we disclose any information to outside parties?</strong></p>" +
          "<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>" +
          "<p><srong>Third party links</p>" +
          "<p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>" +
          "<p><strong>Online Privacy Policy Only</strong></p>" +
          "<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>" +
          "<p><strong>Your Consent</strong></p>" +
          "<p>By using our site, you consent to our web site privacy policy.</p>"+
          "<p><strong>Changes to our Privacy Policy</strong></p>" +
          "<p>If we decide to change our privacy policy, we will post those changes on this page.</p>",
      'PRIVACY_TEXT_2':   'Attn: Data Privacy Officer<br><br>' +
          'DIGATRADE LTD<br>' +
          '838 West Hastings Street, Suite 300<br>' +
          'Vancouver, BC Canada V6C-0A6<br>' +
          'Tel: +1(604) 200-0071<br>' +
          'Email: info@digatrade.com',
      'SECURITY_TEXT1_1': "The cornerstone of ANX, providing you a peace of mind",
      'SECURITY_TEXT1_2': "",
      'SECURITY_TEXT1_3': "All customer funds are segregated and cold storage is utilised to prevent against theft or loss.<br>We do not operate a fractional reserve and all funds are accounted for through frequent reconciliations. Additionally all funds are subject to an annual audit conducted by a licensed third party auditor.",
      'SECURITY_TEXT2_H1':"3-Factor Authentication",
      'SECURITY_TEXT2_1': "2 step process to login,<br>3 levels of verification for withdrawals",
      'SECURITY_TEXT2_2': "<p>Provides extra protection to prevent your account from unauthorized access.</p>" +
          "<p>Other than your regular password, you will be asked to enter a real time password generated by Google Authenticator. To provide you an extra level of security, when you withdraw, you will need to activate a link on the verification email to complete the request.</p>",
      'SECURITY_TEXT3_H1':"SSL Encryption",
      'SECURITY_TEXT3_1' :"We use 128-bit encryption to encrypt all communication between you and our website. This is the highest encryption available and is used as the gold standard for all secure communication on the net.",
      'SECURITY_TEXT3_H2':"Passwords",
      'SECURITY_TEXT3_2' :"We do not use MD5 hashing to encrypt your password. To avoid common weaknesses, our proprietary procedures are designed to provide you, our valued clients, with the peace of mind that comes from our Next Level security implementation.",
      'SECURITY_TEXT3_H3':"State of the Art Infrastructure",
      'SECURITY_TEXT3_3' :"The ANXBTC.COM platform is hosted in Tier 3+ ISO 27001/9001 compliant data centers. Digital currencies are not stored with cloud providers.",
      'SECURITY_TEXT3_H4':"DDOS Protection",
      'SECURITY_TEXT3_4' :"We leverage one of the world's strongest forms of protection against Distributed Denial of Service attacks. We do not pretend to do this by ourselves and partner with multiple third parties who have proven to mitigate some of the largest DDOS attacks in Internet history.",
      'SECURITY_TEXT3_H5':"DB Security",
      'SECURITY_TEXT3_5' :"Our databases are encrypted and protected against SQL injection attacks. We also do hourly backups where we send the backups off site to multiple locations. Everything is fully logged and we are able database if needed.",
      'SECURITY_TEXT3_H6':"Multi-Firewall Protection",
      'SECURITY_TEXT3_6' :"We closely monitor all incoming and outgoing traffic on a very stringent manner to ensure we prevent our network from malicious attack and injection as well as data threats.",
      'SECURITY_TEXT4_H1':"Business Continuity Planning",
      'SECURITY_TEXT4_1' :"We have process and controls in place to deal with outages or attacks. Emails will be sent out to notify you of alternative ways to get back into our site. Our site and funds are totally segregated so you can be assured your funds are safe with us.",
      'SECURITY_TEXT4_H2':"Regular Stress & Soak Testing",
      'SECURITY_TEXT4_2' :"Our technology is immediately scalable. Our regular stress testing has proven it achieves low latency processing and we've soak tested to over 10 million transactions within a 24 hour period. Translation: our engine and underlying infrastructure can handle load, and lots of it.",
      'SECURITY_TEXT5_H1':"Compliance Framework",
      'SECURITY_TEXT5_1' :"We insist on a comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) compliance framework. This includes the monitoring of suspicious transactions and obligatory reporting to local regulators and other compliance bodies.",
      'SECURITY_TEXT5_2' :"Our AML and KYC policies differ depending on the country of origin of which our clients are located and furthermore recorded through the ANXBTC.COM registration process. Our specific policies are detailed within our Terms of Use and which you must accept as per the new user registration process. Our robust compliance framework ensures that regulatory requirements are being adhered to at both a local and global level, providing the highest levels of trust and ensuring ANXBTC.COM continues to operate reliably for the long term.",
      'STORE_SUBHEAD':    "Sai Ying Pun Branch<br>Hong Kong",
      'STORE_TEXT1_1':    "Shop 11C, G/F, 118 Connaught Road West, Hong Kong<br>t +852 2548 2078<br>f +852 2892 2635<br>support@anxintl.com",
      'STORE_TEXT1_2':    "Monday to Friday, 11am to 6pm",
      'STORE_TEXT1_3':    "Get Directions",
      'STORE_TEXT2_H1':   "Services:",
      'STORE_TEXT2_H2':   "Verification",
      'STORE_TEXT2_H3':   "Deposit",
      'STORE_TEXT2_H4':   "Buy",
      'TERMS_TEXT_H1':    "Terms",
      'TERMS_TEXT_H2':    "KYC & AML",
      'TERMS_TEXT_H3':    "Disclaimer",
      'TERMS_TEXT_1':     "<p>In using this website you are deemed to have read and agreed to the following terms of use:</p>" +
          "<p>These Terms and Conditions (the 'Terms') set out the conditions under which DigaTrade Ltd, a company incorporated under the laws of British Columbia, Canada offer you use of the DIGATRADE.COM Website at https://DIGATRADE.COM (the 'Site' or 'Website') and access to the DIGATRADE.COM Platform (the 'Platform'). Please read these Terms carefully and do not use the Site or the Platform unless you accept them.</p>" +
          "<p>Furthermore, the following terminology applies to these Terms and Conditions and any or all Agreements: 'Client', 'Registered Member', 'You' and 'Your' refers to you, the person accessing this website and accepting the Company's terms and conditions. 'The Company', 'Ourselves', 'We' and 'Us', refers to DIGATRADE.COM. 'Party', 'Parties', or 'Us', refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment or fees necessary to undertake the services provided by DIGATRADE.COM and its' associated Platform.</p>" +
          "<p>DIGATRADE.COM and its associated Platform facilitates a service whereas registered members can exchange fiat currency for virtual currency.</p>" +
          "<p>By registering on and using any of the functions provided by the DIGATRADE.COM Site or Platform, you are deemed to have read, understood and accepted all of the following terms & conditions. In addition, by opening an account to use the DIGATRADE.COM Platform:</p>" +
          "<ul>" +
          "<li>you have accepted these Terms; and</li>" +
          "<li>you are confirming that you are at least 18 years of age and have the full capacity to accept these Terms and enter into a transaction as a result of using the DIGATRADE.COM Platform</li>" +
          "<li>you agree only to trade with legally obtained funds that belong to you</li>" +
          "<li>you agree to take full responsibility for your trading or non-trading actions and any gains or losses sustained as a result of using the DIGATRADE.COM platform</li>" +
          "<li>you confirm the details provided upon registration are true and accurate</li>" +
          "<li>you agree to abide by any relevant laws in your jurisdiction, including reporting any trading profits for taxation purposes.</li>" +
          "</ul>" +
          "<p>Changes to Terms</p>" +
          "<p>DigaTrade Ltd reserves the right, at its sole discretion, to change, add or remove portions of these Terms, at any time. Such notification will be made via the Website. It is your responsibility to review the amended Terms. Your continued use of the Website and Platform following the posting of changes will mean that you accept and agree to the changes and you agree that all subsequent transactions by you will be subject to these Terms.</p>" +
          "<p>DigaTrade Ltd reserves the right to close inactive accounts that have exhibited no events or transaction activity for a period greater than 6 months. Customers of whom accounts will be closed as a result of this condition will be contacted by DigaTrade Ltd using the customers registered email address 14 days prior to account closure. After the account is closed, customers will have 30 days to make arrangements for any funds held in the account or notify DigaTrade Ltd by email that they wish to keep the account open. DigaTrade Ltd will not be liable for any funds that were held within the account after 30 days from account closure.</p>",
      'VAULT_HEAD2':      'Account Balance at a Glance',
      'VAULT_HEAD3':      'Check Account Activities',
      'VAULT_HEAD4':      'Send and Receive',
      'VAULT_HEAD5':      'Other Features',
      'VAULT_HEAD7':      'World Market Data',
      'VAULT_HEAD8':      'Debit Card Order and Recharge',
      'VAULT_TEXT1':      'The all-in-one mobile wallet to manage your bitcoins',
      'VAULT_TEXT2_H1':   'Quick View',
      'VAULT_TEXT2_1':    "View your latest portfolio value at current exchange rates.",
      'VAULT_TEXT2_H2':   'Portfolio View',
      'VAULT_TEXT2_2':    "View all your assets in one single comprehensive listing.",
      'VAULT_TEXT2_H3':   'Multi-Currencies Support',
      'VAULT_TEXT2_3':    "View all the fiat currencies you currently hold.",
      'VAULT_TEXT2_H4':   "Base Currency Setting",
      'VAULT_TEXT2_4':    "Select the home currency you want your account to be based in.",
      'VAULT_TEXT3_H1':   "Transaction List",
      'VAULT_TEXT3_1':    "View latest transactions on your account with details such as date, time and exchange rates.",
      'VAULT_TEXT3_H2':   "Pending Transactions",
      'VAULT_TEXT3_2':    "Pending transactions are listed on a separate tab with real time updates on the number of confirmations received before the transaction is fully completed.",
      'VAULT_TEXT4_H1':   "Send Bitcoins",
      'VAULT_TEXT4_1':    "To send bitcoins, simply enter the amount of bitcoins and the recipient's bitcoin wallet address or QR code. If the person does not currently own a bitcoin wallet, you can just enter their email address!",
      'VAULT_TEXT4_H2':   "Receive Bitcoins",
      'VAULT_TEXT4_2':    "Save the bitcoin addresses of your friends and family into your address book and send them bitcoins any time you want.",
      'VAULT_TEXT5_H1':   "Alert Setup",
      'VAULT_TEXT5_1':    "Set up alerts so that you get email notifications when your desired price is reached!",
      'VAULT_TEXT5_H2':   "Address Book",
      'VAULT_TEXT5_2':    "Save the bitcoin addresses of your friends and family into your address book and send them bitcoins any time you want.",
      'VAULT_TEXT6_1':    "Get this mobile wallet now, available on:",
      'VAULT_TEXT7_H1':   "World Markets",
      'VAULT_TEXT7_1':    "Get real time market data from the world’s most popular Bitcoin exchanges. The information is accessible to everyone and does not require an ANX account.",
      'VAULT_TEXT8_H1':   "Order Cards",
      'VAULT_TEXT8_1':    "Choose your preferred debit card and delivery option then place your order.",
      'VAULT_TEXT8_H2':   "Recharge Cards",
      'VAULT_TEXT8_2':    "To add funds to your card, simply enter the recharge amount in Bitcoin and it will be transferred directly into your debit card.",
  });

}]);



angular.module('app.trade', ['anxApp.trader']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/trade', {
    templateUrl: 'trader/trader.html'
  });
}]).run(["RoutePermissionsService", function(RoutePermissionsService) {
  return RoutePermissionsService.requireAuth('/trade');
}]);

angular.module('app.trade', ['anxApp.trader']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/trade', {
    templateUrl: 'trader/trader.html'
  });
}]).run(["RoutePermissionsService", function(RoutePermissionsService) {
  return RoutePermissionsService.requireAuth('/trade');
}]);

angular.module('app.translations', ['anxApp.translations']);

angular.module('app.translations').config(["LanguageServiceProvider", function(LanguageServiceProvider) {
  return LanguageServiceProvider.registerLanguage('en_US', {
    SITE_TITLE_ANXPRO: 'ANXPRO Bitcoin Exchange'
  });
}]);

angular.module('anxApp.translations').config(['LanguageServiceProvider', function(LanguageServiceProvider) {
	LanguageServiceProvider.registerLanguage('en_US', {"annualIncome.label.0+":"$0 USD - $50,000 USD","annualIncome.label.400+":"$50,001 USD - $100,000 USD","annualIncome.label.800+":"$100,001 USD - $150,000 USD","annualIncome.label.1200+":"$150,000 USD and above"});

	LanguageServiceProvider.registerLanguage('en_US', {"footer.label.copyright":"Copyright © DigaTrade 2015","footer.label.disclaimer":"DISCLAIMER: Bitcoin and crypto currencies prices are volatile and fluctuate day-to-day. Trading in these crypto currencies may be considered a high risk activity. Proper and sound judgement should be used in evaluating the risks associated with these activities. DigaTrade Ltd does not solicit nor make any representation that crypto currencies are an investment vehicle. The decision to trade crypto currencies rests entirely on the users own independent judgement."});

	LanguageServiceProvider.registerLanguage('en_US', {"home.slide1.title":"Be Free<br>Buy Bitcoins<br>Be Your Own Bank"});
}]);
angular.module('anxApp.translations').config(['LanguageServiceProvider', function(LanguageServiceProvider) {
	LanguageServiceProvider.registerLanguage('en_US', {"annualIncome.label.0+":"$0 USD - $50,000 USD","annualIncome.label.400+":"$50,001 USD - $100,000 USD","annualIncome.label.800+":"$100,001 USD - $150,000 USD","annualIncome.label.1200+":"$150,000 USD and above"});

	LanguageServiceProvider.registerLanguage('en_US', {"footer.label.copyright":"Copyright © DigaTrade 2015","footer.label.disclaimer":"DISCLAIMER: Bitcoin and crypto currencies prices are volatile and fluctuate day-to-day. Trading in these crypto currencies may be considered a high risk activity. Proper and sound judgement should be used in evaluating the risks associated with these activities. DigaTrade Ltd does not solicit nor make any representation that crypto currencies are an investment vehicle. The decision to trade crypto currencies rests entirely on the users own independent judgement."});

	LanguageServiceProvider.registerLanguage('en_US', {"home.slide1.title":"Be Free<br>Buy Bitcoins<br>Be Your Own Bank"});
}]);
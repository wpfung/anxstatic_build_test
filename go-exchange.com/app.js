angular.module('app', ['anxApp', 'anxApp.home', 'app.home', 'app.translations', 'anxApp.stories', 'anxApp.events', 'app.simpleTrade', 'inline']);

angular.module('app').controller('AppController', ["$scope", "$controller", "$translate", function($scope, $controller, $translate) {
  $controller('ApplicationController', {
    $scope: $scope
  });
  this.translation = function(key) {
    return $translate.instant(key);
  };
  this.translationEmpty = function(key) {
    var result;
    result = this.translation(key);
    if (typeof result === 'string' && result === '') {
      return true;
    }
    return true;
  };
  return this;
}]);

angular.module('app.home', ['ngRoute', 'templates']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/', {
    templateUrl: 'home/home.html'
  }).when('/home/announcements', {
    templateUrl: 'home/home.html'
  }).when('/home/market', {
    templateUrl: 'home/home.html'
  }).when('/home/press', {
    templateUrl: 'home/home.html'
  });
}]);

angular.module('anxApp.home').controller('HomeSliderController', ["$scope", function($scope) {
  var getIndexes, that;
  that = this;
  this.slides = eval('(' + $scope.slides + ')');
  getIndexes = function() {
    var k, result;
    result = [];
    for (k in that.slides) {
      result.push(k);
    }
    return result;
  };
  this.indexes = getIndexes();
  return this;
}]);

angular.module('anxApp.home').directive('homeSlider', ["$templateCache", function($templateCache) {
  return {
    restrict: "E",
    replace: true,
    template: $templateCache.get('home/home_slider.html'),
    scope: {
      slides: "@slides"
    },
    controller: 'HomeSliderController',
    controllerAs: 'slider'
  };
}]);

/**
 * Created by albert on 8/7/14.
 */

angular.module('anxApp.pages').config(['$translateProvider', function ($translateProvider) {

  $translateProvider.translations('en_US', {
      'ABOUT-US':         'About Us',
      'TEAM':             'Team Profile',
      'PRIVACY':          'Privacy Policy',
      'SOCIAL':           'Social',
      'HELP':             'Help',
      'FAQ':              'FAQ',
      'API':              'API Reference',
      'TERMS':            'Terms of Use',
      'CONTACT':          'Contact Us',
      'CONTACTS':         'Contacts',
      'PARTNER':          'Partnership',
      'JOBS':             'Jobs',
      'SITE_NAME':        'DIGATRADE',
      'HEAD_ABOUT':       'About Go Exchange',
      'HEAD_BUYSELL':     'Purchase EGD',
      'HEAD_CAREERS':     'Careers',
      'HEAD_CRYPTO':      'Buy/Sell Altcoins',
      'HEAD_DISCLAIM':    'Disclaimer',
      'HEAD_FEES':        'Fees',
      'HEAD_KYC':         'KYC & AML',
      'HEAD_PRESS':       'In the Press',
      'HEAD_PRIVACY':     'Privacy Policy',
      'HEAD_VAULT':       'EGDs on the Go',
      'HEAD_SECURITY':    'Security',
      'HEAD_SERVICES':    'Services',
      'HEAD_START':       'Getting Started',
      'HEAD_STORE':       'ANX Retail Store',
      'HEAD_TERMS':       'Terms of Use',
      'HEAD_TANDC':       'Terms & Conditions',
      'HEAD_INVESTORS':   'Investor Information',
      'FOOTER_VAULT':     'Mobile Wallet',
      'BUTTON_LOGIN':     "Log In",
      'BUTTON_REGISTER':  "Register",
      'BUTTON_SIGNUP':    "Sign Up for Free",
      'ABOUT_MORE_1':     "Find out more about",
      'ABOUT_TEXT1_1':
          "<p><b>Go Exchange</b> offers an easy, secure, and affordable platform to buy and sell Crypto Currencies. Go Exchange offers a 24 hour online exchange that provides the automated matching of orders between its registered members and it strives to be your Crypto Currencies connection by making the experience as effortless as possible.</p>" +
          '<h5 class="branded">EGD and All-win business model</h5>' +
          "<p>EGD is one kind of decentralized distributed crypto asset, based on open source network protocol to achieve global P2P transmission in seconds with nearly 0 cost. It is positioned as global integrated business appreciation loyalty points. Compare with traditional loyalty points, EGD was born with features of limited total volume, clear ownership, permanent existence and long-term appreciation. It will become the connection of sharing profits between merchants and consumers after replacing the traditional isolated loyalty points issued by different merchants.</p>" +
          "<p>In the near future, merchants from different regions and industries will take proportion of incomes to purchase this limited total volume EGD points to reward their consumers. When merchants run out of the EGD in their hands, they have to buy the EGD back from consumers with a higher price. During this process, cost for buying EGD whatever with a higher or lower price is came from consumers that means merchants barely had loss. While consumers could enjoy the appreciation of EGD in their pockets to exchange higher profits. This consequent circulation will stimulate potential social consumption vitality and motivate vast consumption demands to build a new integrated pattern of global all-win business model.</p>" +
          "<p>Today, over developed productivity has pushed the human society from one of scarcity to era of overproduction. “Business Clone”, the traditional profits model has gradually lost its magic power. Consumption power is replacing productivity to be the driven force of market economy development. EGD by its feature of scarcity and appreciation could alter the independent operation and vicious competition of merchants from different walks. Meanwhile, it will hold back the trend of increasingly gap between merchants and consumers on wealth.</p>" +
          "<p>As a non-currency encrypted asset, EGD has been absorbed into the taxation system in many countries. It has been working closely with entity industries, such as food, catering, electronics, exhibitions and so on. Later on, it will play a greater role in education, health care, nursing home, agriculture, real estate and securities industries etc.",
      'ADV_SUBHEAD':      "reasons why we should be your EGD connection",
      'ADV_H1_1':         "Safe & Secure",
      'ADV_H2_1':         "Legal & Trustworthy",
      'ADV_H3_1':         "High Liquidity",
      'ADV_H4_1':         "Customer Support",
      'ADV_H5_1':         "No Fees",
      'ADV_H6_1':         "Immediate Processing",
      'ADV_H7_1':         "Mulitple Payment Methods",
      'ADV_H8_1':         "Multi-currencies Support",
      'ADV_1_1':          "<p>Security is the cornerstone of Go-exchange. Here are just a a few factors we use to give you a peace of mind<ul>" +
          "<li>Supports 3-Factor Authentication</li>" +
          "<li>Cold storage is utilised to prevent against theft or loss</li>" +
          "<li>Segregated customer funds and do not operate a fractional reserve</li>" +
          "<li>Partnered with the world’s leading DDOS Service and Hosting Providers</li>" +
          "<li>Multi-firewall protection</li>" +
          "<li>Servers are hosted in a Tier 3+ ISO 27001/9001 compliant data centre</li></ul>" +
          "<p>If you would like to know more about our security provisions, please click here.</p>",
      'ADV_2_1':          "<p>Go-exchange.COM is committed in providing a safe, globally-compliant and reputatable service to you. We pride ourselves on the integrity and transparency of our business. We are:" +
          "<ul><li>A globally recognized brand and top 6 EGD exchange in the world</li>" +
          "<li>Formally registered company in Hong Kong since June 2013 and a fully licensed Money Services Operator (MSO), the first MSO licensed exchange in Asia</li>" +
          "<li>The founding partners have financial markets, management consulting, banking technology and compliance backgrounds. They are well and regulations, especially KYC and AML obligations.</li></ul></p>",
      'ADV_3_1':          "<p>We constantly strive to improve liquidity and tighten our spreads to provide the best price in the EGD market ensuring your potential profits is maximized." +
          "<ul><li>Top 6 largest EGD exchanges in the world by volume</li>" +
          "<li>The only global platform that is able to process trades from the two largest EGD markets: USA and China</li>" +
          "<li>Tight spreads and high liquidity</li></ul></p>",
      'ADV_4_1':          "<p>Our customer support teams at Go-exchange are trained in all aspects of operations and security and most importantly remain EGD loyalists. We offer:" +
          "<ul><li>Multilingual support (English, Traditional Chinese, Simplified Chinese, Portugese)</li>" +
          "<li>Our customer support Key Performance Indicator is to respond to and resolve all inquiries within 12 hours of inquiry</li></ul>",
      'ADV_5_1':          "<p>You are not charged transaction fees when using Go-exchange to buy and sell E-gold.The price displayed is the total price and your account will be credited and debited as per the details provided on screen." +
          "<ul><li>No trading fees</li>" +
          "<li>No bank transfer deposit fees</li>" +
          "<li>No EGD transfer fees</li>" +
          "<li>No additional or hidden fees</li></ul>" +
          "<p>Please see fee schedule for details.</p>",
      'ADV_6_1':          "<p>Our support team promptly check all requests to make sure we faciliate your account effectively and efficiently" +
          "<ul><li>Immediate processing of crypto currency deposits and withdrawals</li>" +
          "<li>Real time processing of fiat deposits and withdrawals</li></ul>",
      'ADV_7_1':          "<p>We provide multiple payment methods for you to efficiently fund your account.<br>" +
          "We have <strong>local banking presence in Hong Kong, China and Australia</strong>.<br>" +
          "Simply fund your account via any of the following methods:</p>",
      'ADV_7_2':          "ATM Cash Deposit",
      'ADV_7_3':          "SEPA",
      'ADV_7_4':          "In-store Cash Deposit",
      'ADV_7_5':          "International Bank Wire",
      'ADV_7_6':          "Local Bank Transfer",
      'ADV_7_7':          "ZipZap",
      'ADV_8_1':          "<p>We support buying and selling EGDs in all 10 global fiat currencies:</p>" +
          "",
      'ADV_9_1':          "Find out more about",
      'ADV_9_2':          "Our Security",
      'BTN_COMPARE':      "Compare Cards <i class='fa fa-credit-card'></i>",
      'BTN_GETONE':       "Get One Now",
      'BUYSELL_SUBHEAD':  "purchasing EGD has never been easier",
      'BUYSELL_TEXT1_H1': "Instant Purchase",
      'BUYSELL_TEXT1_1':  "Provides you with a no fuss approach to manage your EGDs! You are no longer required to understand bids, asks, or exotic order types in order to buy and sell EGDs",
      'BUYSELL_TEXT2_H1': "Purchase EGD",
      'BUYSELL_TEXT2_1':  "Simply fund your Go-exchange account through any of the supported deposit methods and using the instant-buy feature, enter the amount of EGD you wish to purchase and click the Place Order button. The total price debited from your account will be displayed for 30 seconds for you to confirm your purchase. If you do not confirm your purchase within 30 seconds, a new updated price will be displayed. Once confirmed, EGDs are instantly delivered to your Go-exchange wallet. Your EGDs can be immediately withdrawn without delay should you wish to store your EGDs on an alternative wallet",
      'BUYSELL_TEXT2_H2': "",
      'BUYSELL_TEXT2_2':  "",
      'BUYSELL_TEXT2_H3': "Competitive Price",
      'BUYSELL_TEXT2_3':  "To ensure the most competitive pricing for you, Go-exchange has introduced the Go-exchange Dealer Network, exclusively developed for the Go-exchange platform. When a buy or sell order is submitted, the Go-exchange platform receives matching orders from an extensive number of buyers and sellers who have registered as part of the Go-exchange Dealer Network. The Go-exchange platform instantly determines the best price and displays this on screen for 30 seconds each time you submit an order.",
      'BUYSELL_TEXT2_H4': "No Transaction Fees",
      'BUYSELL_TEXT2_4':  "You are no longer charged transaction fees when using the Go-exchange platform to buy and sell EGDs. The price displayed for buying and selling EGDs is determined by the members of the Go-exchange Dealer Network who are actively participating at the time your order is submitted. The price displayed is the total price and your account will be credited and debited as per the details provided on screen. There are no additional or hidden fees",
      'BUYSELL_TEXT3_H1': "Immediate Processing",
      'BUYSELL_TEXT3_1':  "buy and sell orders are processed instantly",
      'BUYSELL_TEXT3_H2': "Safe & Secure",
      'BUYSELL_TEXT3_2':  "With the use of 3-Factor Authentication, we provide extra protection to prevent unauthorized access to your account. We hold our funds in cold storage to ensure your EGDs are safely kept in your Go-exchange wallet.",
      'BUYSELL_TEXT3_H3': "Multiple Payment Methods",
      'BUYSELL_TEXT3_3':  "Support for International Bank Wire, Local Bank Transfer, In-Store and ATM Cash Deposit",
      'BUYSELL_TEXT3_H4': "Multi-currencies & EGD Trading",
      'BUYSELL_TEXT3_4':  "Fund your account in all major currencies: USD, EUR, HKD, AUD, CAD, JPY, SGD, GBP and NZD",
      'BUYSELL_TEXT4_1':  "or",
      'BUYSELL_TEXT4_2':  "to start trading now!",
      'BUYSELL_TEXT5_1':  "If you prefer to trade your EGDs at pricing and times of your choice, try our acclaimed ANXPRO Exchange. With Algo support, Altcoins and much more-this is the one place to handle all your cryptocurrency needs! ",
      'CAREERS_TEXT1_1':  "",
      'CAREERS_TEXT2_H1': "Current Vacancies",
      'CAREERS_TEXT2_1':  "Forum Manager | 2014-08-08 | Permanent<br>Front End Developer | 2014-08-08 | Permanent<br>UX Developer | 2014-08-08 | Permanent<br>",
      'CRYPTO_TEXT1_1':   "A full featured exchange platform that allows you to buy and sell EGDs and other Altcoins. The platform is simple enough for beginners and sophistricated enough for professional traders. ",
      'CRYPTO_TEXT2_H1':  "Peer to Peer Crypto-currencies Support",
      'CRYPTO_TEXT2_1':   "EGD was introduced in January 2009 by an unknown person using the alias Satoshi Nakamoto.  It is the first decentralized peer-to-peer payment network that is powered by its users with no central authority or middlemen. From a user perspective, EGD is pretty much like cash for the Internet.  For more information about EGD, visit our FAQ section.",
      'CRYPTO_TEXT2_2':   "Litcoin was introduced in October 2011 and is nearly identical to EGD except that it has a higher total of coins (84 million) and processes a block every 2.5 minutes rather than EGD's 10 minute interval. it also uses scrypt in its proof-of-work algorithm so there is not as much of an advantage to miners using ASIC mining equipment.",
      'CRYPTO_TEXT2_3':   "Dogecoin was introduced in December 2013 and had a fast initial coin production schedule with approximately 100 billion coins in circulation by the end of 2014 and with an additional 5.2 billion coins every year thereafter. It has gained traction in the social space and as an internet tipping coin.",
      'CRYPTO_TEXT2_4':   "Namecoin was introduced in April 2011 and is recognized as the first fork of the EGD software. Aside form being a crypto-currency, it also acts as an alternative decentralized DNS, which would avoid domain name censorship.",
      'CRYPTO_TEXT2_5':   "Peercoin was introduced in August 2012 and is known as a hybrid proof-of-stake/proof-of-work coin. Unlike other coins, there is no hard limit to the amount that can be mined and instead has a limited release rate plus 1% decentralized inflation built in.",
      'CRYPTO_TEXT2_6':   "Stellar was introduced in August 2014 and it is part of a nonprofit organization, the Stellar Development Foundation, to encourage the wider adoption of digital currencies through the offering of a decentralized gateway for digital currency-to-fiat currency transfers, as well as featuring itself as an internal digital currency of its own.",
      'CRYPTO_TEXT2_7':   "Ripple was first introduced in 2004 by Ryan Fugger and later further developed by Jed McCaleb in 2011. It is the name of both a digital currency (XRP) and an open payment network within which that currency is transferred. It is a distributed, open-source payments system that is still in its beta. As of 2014, Ripple is the second largest cyrptocurrency by market capitalization after EGD and Litecoin.",
      'CARD_SUBHEAD':     "let your EGDs do everything your credit cards can do",
      'CARD_TEXT_Q':      'Questions? Contact us at <a href="mailto:support@go-exchange.com">support@go-exchange.com</a>',
      'CARD_TEXT1_0':     "* Card design is for demonstration only. The actual debit card design will be different.",
      'CARD_TEXT1_H1':    "Transfer your EGDs directly to your Go-exchange debit card anytime anywhere!",
      'CARD_TEXT1_1':     "Automatically links to your Go-exchange account so that you can conveniently fund your debit card for everyday use.",
      'CARD_TEXT2_H1':    "Spend it anywhere in the world just like a credit card",
      'CARD_TEXT2_H2':    "Global ATM Network",
      'CARD_TEXT2_2':     "Withdraw cash anytime anywhere you like. The card can be used at over 25 Million locations worldwide",
      'CARD_TEXT2_H3':    "Point of Sale Purchases",
      'CARD_TEXT2_3':     "Make POS purchases at any brick and mortar stores in every corner of the world!",
      'CARD_TEXT2_H4':    "Online Shopping Worldwide",
      'CARD_TEXT2_4':     "Use it for online transactions and purchases around the world.",
      'CARD_TEXT2_H5':    "Say no to paperwork, application process and credit check!",
      'CARD_TEXT3_1':     " to get your Go-exchange EGD Debit Card today, it's easy, secure and affordable!",
      'CARD_TEXT3_2':     "<a href=\"/pages/card\" ng-click=\"clicked('login')\">Login</a> or <a href=\"/register\">Sign up</a> to get your Go-exchange EGD Debit Card today!",
      'CARD_TEXT4_H1':    "SAFER THAN CARRYING CASH",
      'CARD_TEXT4_1':     "If your card is lost or stolen, you can put a block on it and recover your money. Something you can’t do with cash!",
      'CARD_TEXT4_H2':    "MORE CONVENIENT THAN CASH",
      'CARD_TEXT4_2':     "No more fumbling with cash when making purchases and say no to exchanging money when going abroad, just use your card!",
      'CARD_TEXT4_H3':    "LIMITLESS FLEXIBILITY",
      'CARD_TEXT4_3':     "Get your choice of currency and enjoy benefits at participating merchants.",
      'CARD_TEXT4_H4':    "ACCEPTED WORLDWIDE",
      'CARD_TEXT4_4':     "Go-exchange EGD Debit Cards are part of a global payment network with over 25 million merchants.  You can make purchase at any of these merchants or withdraw cash on their ATM network worldwide.",
      'CARD_TEXT4_H5':    "MANAGE YOUR MONEY ANYWHERE",
      'CARD_TEXT4_5':     "With your Go-exchange account, you can check your card balance and account information on the web or through your mobile phone.",
      'CARD_TEXT4_H6':    "EASY TO GET",
      'CARD_TEXT4_6':     "Getting an Go-exchange EGD Debit Card is extremely easy! There is no paperwork, no troublesome application process and no credit check required.",
      'CARD_TEXT5_H1':    "FEATURED CARDS",
      'CARD_TEXT5_1':     "With Go-exchange EGD Debit Cards, you can enjoy an exciting world of offers, benefits and convenience. Go-exchange offers various debit card options to make sure you get the card that is best suited for your everyday use.",
      'CARD_TEXT5_H2':    "EGD Plus",
      'CARD_TEXT5_2':     "<ul><li>Based in HKD</li>" +
          "<li>Online purchasing</li>" +
          "<li>Online account management</li>" +
          "<li>No monthly fee</li></ul>",
      'CARD_TEXT5_H3':    "EGD Premium",
      'CARD_TEXT5_3':     "<ul><li><strong>No loading fees</strong></li>" +
          "<li>Based in USD</li>" +
          "<li>Free POS purchases</li>" +
          "<li>Speed loading</li></ul>",
      'CARD_TEXT5_4':     'We offer special pricing on bulk orders (minimum of 10 cards), for more details please contact <a href="mailto:support@go-exchange.com">support@go-exchange.com</a>',
      'CARD_TEXT6_H1':    "How it Works",
      'CARD_TEXT7_H1':    "Get A Card",
      'CARD_TEXT7_1':     "Acquire a debit card from Go-Exchange.",
      'CARD_TEXT7_H2':    "Load",
      'CARD_TEXT7_2':     "Transfer EGDs to add funds to your card.",
      'CARD_TEXT7_H3':    "Use",
      'CARD_TEXT7_3':     "Use your card to get cash or make purchases anywhere.",
      'CARD_TEXT7_H4':    "Reload",
      'CARD_TEXT7_4':     "Add more funds to your card via our online portal.",
      'CARD_TABLE1_H1':   "Features",
      'CARD_TABLE1_H2':   "EGD Basic",
      'CARD_TABLE1_H3':   "EGD Premium",
      'CARD_TABLE1_H4':   "EGD Plus",
      'CARD_TABLE1_H5':   "Card Parameters",
      'CARD_TABLE1_H6':   "Fees & Charges",
      'CARD_TABLE1_H7':   "Help & Support",
      'CARD_TABLE1_H8':   "Rewards & Offers",
      'CARD_TABLE1_R1_1': "Card Currency",
      'CARD_TABLE1_R1_2': "USD",
      'CARD_TABLE1_R1_3': "USD",
      'CARD_TABLE1_R1_4': "HKD",
      'CARD_TABLE1_R2_1': "No Credit Check",
      'CARD_TABLE1_R3_1': "ATM Withdrawals",
      'CARD_TABLE1_R4_1': "POS Purchases",
      'CARD_TABLE1_R5_1': "Online Purchases",
      'CARD_TABLE1_R6_1': "Mobile App",
      'CARD_TABLE1_R6_3': "Coming Soon",
      'CARD_TABLE1_R7_1': "Online Account Management",
      'CARD_TABLE1_R7_3': "Coming Soon",
      'CARD_TABLE1_R7_4': "Coming Soon",
      'CARD_TABLE1_R8_1': "Speed Loading",
      'CARD_TABLE1_R8A_1': "International Merchant Network",
      'CARD_TABLE1_R8B_1': "Card Reloadable",
      'CARD_TABLE1_R9_1': "Maximum Card Balance",
      'CARD_TABLE1_R10_1':"Maximum Amount per card load",
      'CARD_TABLE1_R11_1':"ATM Withdrawals",
      'CARD_TABLE1_R11_3':"No Limit, Subject to limits on local ATM",
      'CARD_TABLE1_R11_4':"US$1000, Subject to limits on local ATM",
      'CARD_TABLE1_R12_1':"POS Purchases",
      'CARD_TABLE1_R13_1':"Online Transactions",
      'CARD_TABLE1_R14_1':"ATM Withdrawals",
      'CARD_TABLE1_R14_4':"1%, Min. US$3",
      'CARD_TABLE1_R15_1':"POS Purchases",
      'CARD_TABLE1_R16_1':"ATM Balance Inquiry",
      'CARD_TABLE1_R16_3':"Free",
      'CARD_TABLE1_R17_1':"Monthly Fee",
      'CARD_TABLE1_R17_2':"Free",
      'CARD_TABLE1_R17_3':"Free",
      'CARD_TABLE1_R17_4':"Free for first 12 months<br>HK$5/month thereafter<br>deducted from card balance",
      'CARD_TABLE1_R18_1':"Card Load Fee",
      'CARD_TABLE1_R18_2':"US$5 + 2.5% of recharge amount",
      'CARD_TABLE1_R18_3':"Free",
      'CARD_TABLE1_R18_4':"Free",
      'CARD_TABLE1_R19_1':"FAQs",
      'CARD_TABLE1_R20_1':"Telephone",
      'CARD_TABLE1_R21_1':"Email",
      'CARD_TABLE1_R22_1':"Welcome Offers",
      'CARD_TABLE1_R22_2':"Get a refund on your shipping fee when you charge the required amount on your first loading",
      'CARD_TABLE1_R22_3':"load HK$8,000",
      'CARD_TABLE1_R22_4':"load US$5,000",
      'CARD_TABLE1_R23_1':"Refer a Friend",
      'CARD_TABLE1_R23_2':"Get cash bonus when you refer a friend",
      'CARD_TABLE1_R24_1':"One for One",
      'CARD_TABLE1_R24_2':"",
      'CARD_TABLE1_R25_1':"Merchant Offers",
      'CARD_TABLE1_R25_2':"Enjoy exclusive savings at participating merchants",
      'CARD_TABLE1_R26_1':"Holiday Offers",
      'CARD_TABLE1_R26_2':"Get a FREE EGD Plus card when you verify your account by January 31 2015",
      'CARD_TABLE1_R27_1':"Get a FREE upgrade to EGD Premium when you choose express delivery for receiving verification code",
      'CARD_TABLE1_R28_1':"Order a new EGD Premium card and load 0.9BTC on the first loading to get the card for FREE",
      'CARD_TC_TEXT_H1':  'EGD Basic',
      'CARD_TC_TEXT_H2':  'EGD Plus',
      'CARD_TC_TEXT_H3':  'EGD Premium',
      'CARD_TC_TEXT_1':   '<p>Terms and Conditions for Foreign Currency Prepaid Bearer Card, distributed by Go-exchange as an electronic money instrument</p>' +
          '<p>I. Introductory provisions and definitions</p>' +
          '<p>These Terms and Conditions for Foreign Currency Prepaid Bearer Card, distributed by Go-exchange as an electronic money instrument (“Terms and Conditions”) shall regulate the rules for issuance settlement of Transactions concluded with the Card.</p>' +
          '<p>As used in these Terms and Conditions, the following terms and abbreviations shall mean:</p>' +
          '1. Go-exchange Support Team - The dedicated personnel at Go-exchange whom provide services associated with the issuing and management of the Go-exchange debit card,<br>' +
          '2. Merchant – an entrepreneur who accepts payment for the offered goods or services in non-cash form, with the use of the Card,<br>' +
          '3. Authorisation – the Cardholder’s consent to a Transaction, expressed as set out in these Terms and Conditions,' +
          '4. Bank – The underlying bank associated with the card issuance,' +
          '5. ATM – a device enabling the Cardholder to withdraw cash using the Card, thus withdrawing electronic money from the Card, or to perform other Transactions,' +
          '6. Card Block – temporary blockade imposed by the Bank to prevent the making of Transactions, leading to the blockade of the Available Funds Balance on the Card,' +
          '7. CardMailer – a letter to which the Card is attached at the time of its distribution by Go-exchange,' +
          '8. Business Day – a day other than Saturday or Sunday or a public holiday,' +
          '9. Card – the bearer card, distributed as an electronic money instrument, with the possibility of Card Top-Up,' +
          '10. code / CVV2 code – a three-digit code of the payment organisation placed on the back of the card that is used to authenticate the Card when making Internet, telephone and mail transactions,' +
          '11. PIN – a confidential identification number provided to the Cardholder with the Card, which together with data recorded on the Card is used for electronic identification of the Cardholder,' +
          '12. Transaction Limits – amount or quantity limits set for the individual Transaction types, up to which the Bank authorises the Transactions,' +
          '13. Payment Organisation – the Visa Europe organisation of financial institutions, within which the Bank, on the basis of license held, issues the Cards.' +
          '14. Bank Outlet – corporate branch, corporate office or other outlet of the Bank,' +
          '15. Cardholder – each time the bearer of the Card, who acquired the Card and the CardMailer in a manner permitted under these Terms and Conditions and the generally applicable laws and regulations, thus becoming a party to the Agreement,' +
          '16. Card Account – an account maintained in the Card Account Currency,' +
          '17. Available Funds Balance – the amount of funds stored in the Card Account, up to which Transactions can be made,' +
          '18. POS Terminal – an electronic payment terminal that enables the Cardholder to make Transactions, including a POS Terminal equipped with a contactless reader, enabling contactless transactions by placing the Card in close proximity of the reader,' +
          '19. Transaction – a payment made with the Card or cash withdrawal with the use of the Card, especially from an ATM,' +
          '20. Partial Transaction – payment made with the Card up to the amount of the Available Funds Balance (where the Transaction amount is higher than the Available Funds Balance on the Card) and allowing the payment of the remaining Transaction amount in another manner, including with another payment instrument or with cash,' +
          '21. Contactless Transaction – a non-cash transaction made via a POS Terminal enabling payments by placing the card in the close proximity of the contactless reader,' +
          '22. Remote transaction – a non-cash payment Transaction made without the physical presentation of the Card (over the phone, via the Internet, via mail or by fax),' +
          '23. Agreement – the agreement on the electronic money instrument, concluded at the time of the first Card Top-Up and in force up to the expiry date of the Card. The Agreement’s validity period may not be extended,' +
          '24. Card Account Currency – the currency printed on the CardMailer, in which the Card Account is maintained and the Available Funds Balance is stored. Go-exchange currently distributes the Cards in the following currencies: USD' +
          '25. Card’s Settlement Currency – a currency that is used by the Payment Organisation to settle Transactions made with the Cards, in that case the Card Account Currency,' +
          '26. Buy-Out of Electronic Money – a demand submitted by the Cardholder, which results in the refund of funds kept in the Card Account, under rules set forth in these Terms and Conditions,' +
          '27. Card Top-Up – increasing the Available Funds Balance by transferring funds into the Card Account,' +
          '28. Card Freeze – a situation where the Card is permanently blocked in the Bank’s system and with the Payment Organisation, made at the request of the Cardholder or under the Bank’s and Go-exchange’s decision.',
      'CARD_TC_TEXT_2':   '',
      'CARD_TC_TEXT_3':   '',
      'CONTACT_TEXT_H1':  'Customer Service',
      'CONTACT_TEXT_1':   '<p>Our customer support teams at Go-exchange are trained in all aspects of operations and security and most importantly remain EGD loyalists.</p>' +
          '<p>We are at your service and ready to answer any questions you may have. Our customer support Key Performance Indicator is to respond to and resolve all inquiries within 12 hours of inquiry however in most cases the response time is significantly less. Our commitment to customer service is unmatched and embedded into the DNA of Go-exchange staff. Your feedback is very important to us and we regularly implement new features requested by Go-exchange users.</p>' +
          '<p>Support Contact: <a href="mailto:support@Go-exchange.com">support@Go-exchange.com</a></p>',
      'CONTACT_TEXT_H2':  'Social Channels',
      'CONTACT_TEXT_H3':  'Headquarters',
      'CONTACT_TEXT_3':   'ANX INTERNATIONAL',
      'CONTACT_TEXT_4':   "Unit 701, 7/F, The Sun's Group Centre,<br>200 Gloucester Road, Wanchai Hong Kong<br>Monday to Friday, 9am to 6pm",
      'DISCLAIM_TEXT_1':  "<p>It is your responsibility to determine what, if any, taxes apply to the payments you make or receive, and it is your responsibility to collect, report an remit the correct tax to the appropriate tax authority, regardless of jurisdiction. Go-Exchange is not responsible for determining whether taxes apply to your transaction, or for collecting, reporting or remitting any taxes arising from any transaction, to you or any taxation, governing or third authority.</p>" +
          "<p>Trading EGDs, virtual currencies and virtual commodities carries a high level of risk, and may not be suitable for everyone. Before deciding to buy or sell these instruments you should carefully consider your investment objectives, level of experience, and risk appetite. The possibility exists that you could sustain a loss of some of all of your investment and therefore you should not invest money that you cannot afford to lose. You should be aware of all the risks associated with virtual commodities, and seek advice from an independent financial adviser should you have any doubts. Furthermore, there may be risks that are not disclosed in our Terms of use. You should use extreme consideration and be conscious of assessing your financial situation and tolerance for risk before engaging in activities involving the trading of virtual commodities.</p>" +
          "<p>Any opinions, news, research, analyses, prices, or other information contained on this website is provided as general market commentary, and does not constitute investment advice. Go-Exchange will not accept liability for any loss or damage, including without limitation to, any loss of profit, which may arise directly or indirectly fro use of or reliance on such information.</p>" +
          "<p>There are risks associated with utilizing an Internet-based deal execution trading system including, but not limited to, the failure of hardware, software, and Internet connection. As Go-Exchange does not control the reliability or availability of the Internet, Go-Exchange cannot be responsible for communication failures, distortions or delays when trading via the Internet.</p>" +
          "<p>The content on this website is subject to change at any time without notice, Go-Exchange has taken reasonable measures to ensure the accuracy of the information on the website, however, does not guarantee its accuracy, and will not accept liability for any loss or damage which may arise directly or indirectly from the content or your inability to access the website, for any delay in or failure of the transmission or the receipt of any instruction or notifications sent through website.</p>" +
          "<p>In case of discrepancy between the English version and other language versions in respect of all or any part of the contents in the website, the English version shall prevail.</p>",
      'FEES_TEXT_0':      "<p>You are not charged with transaction fees when using the Go-exchange platform to buy and sell E-gold. The price displayed for buying " +
          "and selling EGDs via Go-Exchange is determined by the members of the Go-exchange Dealer Network who are actively participating at " +
          "the time your order is submitted. The price displayed is the total price and your account will be credited and debited as per the " +
          "details provided on screen. For more information about the Go-Exchange Dealer Network, please click here. There are no additional or hidden fees. Deposit and withdrawal fees charged by Go-Exchange are as follows:</p>",
      'FEES_TEXT_H1':     "Deposit and Withdrawal Fees",
      'FEES_TEXT_1':      '<table class="table table-condensed">' +
          "   <thead>" +
          "       <tr>" +
          "           <th>Funding Type</th>" +
          "           <th>Fee</th>" +
          "       </tr>" +
          "   </thead>" +
          "   <tbody>" +
          "       <tr><td>Deposit via Sepa (EUR)</td><td>1%</td></tr>" +
          "       <tr><td>Deposit via Bank Wire (EUR)</td><td>1%</td></tr>" +
          "       <tr><td>Deposit via Bank Wire (USD)</td><td>1%</td></tr>" +
          "       <tr><td>Deposit via Bank Wire (CAD)</td><td>1%</td></tr>" +
          "       <tr><td>Deposit via Bank Wire (GBP)</td><td>1%</td></tr>" +
          "       <tr><td>Deposit EGD</td><td>Free</td></tr>" +
          "       <tr><td>Withdraw EGD</td><td>Free (Exc. Crypto Network Fee)</td></tr>" +
          "   </tbody>" +
          "</table>" +
          "<p>In some cases your bank or other third party intermediary may charge you additional fees upon wire-in or wire-out operations. These fees are at the discretion of your bank or the third party intermediary and are in no way associated with Go-exchange’s fee schedule.</p>",
      'FEES_TEXT_H2':     "Transfer Limits",
      'FEES_TEXT_2':      '<table class="table table-condensed">' +
          '   <thead>' +
          '       <tr>' +
          '           <th>Account Status</th>' +
          '           <th>Minimum Fiat Currency Deposit</th>' +
          '           <th>Maximum Fiat Currency Deposit</th>' +
          '           <th>Minimum Fiat Card Load</th>' +
          '           <th>Maximum Fiat Card Load</th>' +
          '           <th>Deposit Cryptocurrency</th>' +
          '           <th>Withdraw Cryptocurrency</th>' +
          '       </tr>' +
          '   </thead>' +
          '   <tbody>' +
          '       <tr><td>Unverified</td><td>Not Available</td><td>Not Available</td><td>Not Available</td><td>Not Available</td><td>No Limit</td><td>20 EGD</td></tr>' +
          '       <tr><td rowspan="7">Verified</td><td>USD 500 (or equivalent)</td><td>USD 40,000 (or equivalent)</td><td>USD 100<br>HKD 800</td><td>USD 40,000<br>HKD 200,000</td><td rowspan="7">No Limit</td><td>700 EGD</td></tr>' +
          '   </tbody>' +
          "</table>",
      'FOOTER_ABOUT':     'Go-exchange',
      'HEAD_CARD':        'Go-Exchange Debit Card',
      'KYC_TEXT_1':       "<p>KYC (Know-Your-Customer) and AML (Anti Money Laundering) Policies</p>" +
          "<p>Go-Exchange remains committed in providing a safe, globally-compliant and reputable service to its clients. As a company, we pride ourselves on the integrity and transparency of our business. For this reason, Go-Exchange relies on comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) policies.</p>" +
          "<p>Go-Exchange is legally registered and its infrastructure is physically located in Vancouver, British Columbia, Canada. Go-Exchange insists on a comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) compliance framework. This includes the monitoring of suspicious transactions and obligatory reporting to local regulators and other compliance bodies.</p>" +
          "<p>Our AML and KYC policies differ depending on the country of origin of which our clients are located and furthermore recorded against through the Go-Exchange registration process. The specific AML and KYC policies as per regional jurisdiction are located below. Our robust compliance framework ensures that regulatory requirements are being adhered to at both a local and global level, instilling a level of trust and ensuring Go-Exchange will continue to operate indefinitely.</p>" +
          "<p>Go-Exchange reserves the right to refuse registration to persons from or in jurisdictions that do not meet international AML standards or could be considered as a Politically Exposed Person.</p>" +
          "<p>Go-Exchange reserves the right to carry out customer due diligence to verify its users and their transactions. Enhanced customer due diligence will be carried out periodically as part of our ongoing risk review and assessment. In addition to this, any attempt to abuse Go-Exchange or its platform will result in an immediate account suspension and reported to the respective authorities.</p>" +
          "<p>The Go-Exchange AML and KYC policies are as follows:</p>" +
          "<ul>" +
          "<li>Transactions conducted via the Go-Exchange platform will be subject to AML transaction monitoring.</li>" +
          "<li>Identity and proof of address documents and transaction records will be maintained for at least six years and will be submitted to regulators as required without prior notification to registered users.</li>" +
          "<li>Go-Exchange may at any time without liability and without disclosing any reason, suspend the operation of your account. Go-Exchange shall notify you after exercising its rights under this clause.</li>" +
          "<li>Registered members of whom transactions involve fiat currency will be required to verify their identity and adhere to the Go-Exchange KYC policy. This includes the submission of both government ID and proof of address. Valid ID includes a passport, national card, or drivers license. Valid proof of address includes utility and rates bills not older than 3 months. Other forms of ID and Address verification will not be accepted. Your account will be unregistered until account verification has been completed.</li>" +
          "<li>Suspicious transactions will result in a Suspicious Activity Report being submitted to the Joint Financial Intelligence Unit ('JFIU').</li>" +
          "</ul>",
      'PARTNER_TEXT_1':   "<p>The founders of Go-exchange.COM are approachable in discussing potential opportunities with both institutions and individuals. Please direct initial inquiries to <a href=\"mailto:support@go-exchange.com\">support@go-exchange.com</a></p>",
      'PRIVACY_TEXT_1':   "<p>This Statement is made in accordance with the Personal Data (Privacy) Ordinance (Cap. 486) of the Laws of Hong Kong Special Administrative Region ('the PDPO'). This Statement intends to notify you why your personal data is collected, how it will be used, to whom the personal data will be transferred and to whom data access requests are to be addressed.</p>" +
          "<p><strong>What is the purpose of collecting your information?</strong></p>" +
          "<p>We collect your data to provide our services, enhance our customer service and improve your customer experience. We will indicate in the relevant forms or applications if the required information is for obligatory or voluntary purposes. If information is to be used for an obligatory purpose, you MUST provide your personal data to us if you want us to provide the service for which you are applying. If personal data is only to be used for a voluntary purpose, you can tell us not to use your personal data for that purpose and we will refrain from doing so. You may, however, visit our site anonymously.</p>" +
          "<p>If you have not attained the age of 18 years (being a minor), you must obtain the express consent from your parents or guardian before providing any personal data to us.</p>" +
          "<p><strong>What do we use your information for?</strong></p>" +
          "<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. But any of the information we collect from you may be used in one of the following ways:</p>" +
          "<ul>" +
          "<li>To personalize your experience (your information helps us to better respond to your individual needs)</li>" +
          "<li>To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>" +
          "<li>To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</li>" +
          "<li>To process transactions (Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.)</li>" +
          "<li>To send periodic emails (The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.)</li>" +
          "<li>To meet regulatory and/or compliance obligations (we are required to verify your identity and obtain additional information when your account transacts in fiat currencies)</li>" +
          "</ul>" +
          "<p><strong>Do we transfer your information to third parties?</strong></p>" +
          "<p>Your information held by us will be kept confidential but we may disclose and transfer such data to the following parties (whether within or outside the Hong Kong Special Administrative Region) for respective purposes:</p>" +
          "<ol>" +
          "<li>Professional advisors, service providers, contractors and agents who provide administrative, computer, content, payment, marketing and all relevant services in connection with the operations of our business and the services we provided to you who show to us that they have proper policy, procedures and practice to protect your information disclosed to them by us;</li>" +
          "<li>Debit card issuers who provide debit card services to subscripted customers for the purpose of maintaining their debit card program and related services only; and</li>" +
          "<li>Any person and entities to whom we are under an obligation under law to make disclosure.</li>" +
          "</ol>" +
          "<p><strong>Do we transfer your information for direct marketing?</strong></p>" +
          "<p>We do not use any of your information for direct marketing and we do not transfer any of your information to third party for their direct marketing purposes.</p>" +
          "<p><strong>How do we protect your information?</strong></p>" +
          "<p>We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access your personal information. We offer the use of a secure server. All supplied sensitive information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be accessed only by those authorized with special access rights to our systems, and are required to keep the information confidential.</p>" +
          "<p>After a transaction, your private information will not be kept on file for more than 60 days.</p>" +
          "<p><strong>Do we use cookies?</strong></p>" +
          "<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information. We use cookies to help us remember as well as to process the items in your shopping cart and understand and save your preferences for future visits.</p>" +
          "<p>Most of the Web browsers are initially set up to accept cookies. You can choose to “not accept” cookies by changing the settings but if you do so you may find that certain features on the website do not work properly.</p>" +
          "<p><srong>Third party links</strong></p>" +
          "<p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>" +
          "<p><strong>Changes to our Privacy Policy</strong></p>" +
          "<p>If we decide to change our privacy policy, we will post those changes on this page.</p>" +
          "<p><strong>Retention of Data</strong></p>" +
          "<p>We will keep your personal data for a reasonable time to fulfill the purpose for which the data was collected or comply with the relevant rules, regulations and laws of Hong Kong. Personal data which is no longer required will be destroyed.</p>" +
          "<p><strong>Data Access/Correction Requests</strong></p>" +
          "<p>Under the PDPO, any individual has the right to request access to and correction of his or her personal data held by us. In accordance with the terms of the PDPO, We have the right to charge a reasonable fee for the processing of any data access request.</p>",
      'PRIVACY_TEXT_2': "",
      'SECURITY_TEXT1_1': "The cornerstone of Go-exchange, providing you a peace of mind",
      'SECURITY_TEXT1_2': "",
      'SECURITY_TEXT1_3': "All customer funds are segregated and cold storage is utilised to prevent against theft or loss.<br>We do not operate a fractional reserve and all funds are accounted for through frequent reconciliations. Additionally all funds are subject to an annual audit conducted by a licensed third party auditor.",
      'SECURITY_TEXT2_H1':"3-Factor Authentication",
      'SECURITY_TEXT2_1': "2 step process to login,<br>3 levels of verification for withdrawals",
      'SECURITY_TEXT2_2': "<p>Provides extra protection to prevent your account from unauthorized access.</p>" +
          "<p>Other than your regular password, you will be asked to enter a real time password generated by Google Authenticator. To provide you an extra level of security, when you withdraw, you will need to activate a link on the verification email to complete the request.</p>",
      'SECURITY_TEXT3_H1':"SSL Encryption",
      'SECURITY_TEXT3_1' :"We use 128-bit encryption to encrypt all communication between you and our website. This is the highest encryption available and is used as the gold standard for all secure communication on the net.",
      'SECURITY_TEXT3_H2':"Passwords",
      'SECURITY_TEXT3_2' :"We do not use MD5 hashing to encrypt your password. To avoid common weaknesses, our proprietary procedures are designed to provide you, our valued clients, with the peace of mind that comes from our Next Level security implementation.",
      'SECURITY_TEXT3_H3':"State of the Art Infrastructure",
      'SECURITY_TEXT3_3' :"The Go-exchange.COM platform is hosted in Tier 3+ ISO 27001/9001 compliant data centers. Digital currencies are not stored with cloud providers.",
      'SECURITY_TEXT3_H4':"DDOS Protection",
      'SECURITY_TEXT3_4' :"We leverage one of the world's strongest forms of protection against Distributed Denial of Service attacks. We do not pretend to do this by ourselves and partner with multiple third parties who have proven to mitigate some of the largest DDOS attacks in Internet history.",
      'SECURITY_TEXT3_H5':"DB Security",
      'SECURITY_TEXT3_5' :"Our databases are encrypted and protected against SQL injection attacks. We also do hourly backups where we send the backups off site to multiple locations. Everything is fully logged and we are able database if needed.",
      'SECURITY_TEXT3_H6':"Multi-Firewall Protection",
      'SECURITY_TEXT3_6' :"We closely monitor all incoming and outgoing traffic on a very stringent manner to ensure we prevent our network from malicious attack and injection as well as data threats.",
      'SECURITY_TEXT4_H1':"Business Continuity Planning",
      'SECURITY_TEXT4_1' :"We have process and controls in place to deal with outages or attacks. Emails will be sent out to notify you of alternative ways to get back into our site. Our site and funds are totally segregated so you can be assured your funds are safe with us.",
      'SECURITY_TEXT4_H2':"Regular Stress & Soak Testing",
      'SECURITY_TEXT4_2' :"Our technology is immediately scalable. Our regular stress testing has proven it achieves low latency processing and we've soak tested to over 10 million transactions within a 24 hour period. Translation: our engine and underlying infrastructure can handle load, and lots of it.",
      'SECURITY_TEXT5_H1':"Compliance Framework",
      'SECURITY_TEXT5_1' :"We insist on a comprehensive and thorough KYC (Know-Your-Customer) and AML (Anti Money Laundering) compliance framework. This includes the monitoring of suspicious transactions and obligatory reporting to local regulators and other compliance bodies.",
      'SECURITY_TEXT5_2' :"Our AML and KYC policies differ depending on the country of origin of which our clients are located and furthermore recorded through the Go-exchange.COM registration process. Our specific policies are detailed within our Terms of Use and which you must accept as per the new user registration process. Our robust compliance framework ensures that regulatory requirements are being adhered to at both a local and global level, providing the highest levels of trust and ensuring Go-exchange.COM continues to operate reliably for the long term.",
      'STORE_SUBHEAD':    "Sai Ying Pun Branch<br>Hong Kong",
      'STORE_TEXT1_1':    "Shop 11C, G/F, 118 Connaught Road West, Hong Kong<br>t +852 2548 2078<br>f +852 2892 2635<br>support@go-exchange.com",
      'STORE_TEXT1_2':    "Monday to Friday, 11am to 6pm",
      'STORE_TEXT1_3':    "Get Directions",
      'STORE_TEXT2_H1':   "Services:",
      'STORE_TEXT2_H2':   "Verification",
      'STORE_TEXT2_H3':   "Deposit",
      'STORE_TEXT2_H4':   "Buy",
      'TERMS_TEXT_H1':    "Terms",
      'TERMS_TEXT_H2':    "KYC & AML",
      'TERMS_TEXT_H3':    "Disclaimer",
      'TERMS_TEXT_1':     "<p>In using this website you are deemed to have read and agreed to the following terms of use:</p>" +
                          "<p>These Terms and Conditions (the 'Terms') set out the conditions under which Go-Exchange offer you use of the GO-EXCHANGE.COM Website at https://GO-EXCHANGE.COM (the 'Site' or 'Website') and access to the GO-EXCHANGE.COM Platform (the 'Platform'). Please read these Terms carefully and do not use the Site or the Platform unless you accept them.</p>" +
                          "<p>Furthermore, the following terminology applies to these Terms and Conditions and any or all Agreements: 'Client', 'Registered Member', 'You' and 'Your' refers to you, the person accessing this website and accepting the Company's terms and conditions. 'The Company', 'Ourselves', 'We' and 'Us', refers to GO-EXCHANGE.COM. 'Party', 'Parties', or 'Us', refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment or fees necessary to undertake the services provided by GO-EXCHANGE.COM and its' associated Platform.</p>" +
                          "<p>GO-EXCHANGE.COM and its associated Platform facilitates a service whereas registered members can exchange fiat currency for virtual currency.</p>" +
                          "<p>By registering on and using any of the functions provided by the GO-EXCHANGE.COM Site or Platform, you are deemed to have read, understood and accepted all of the following terms & conditions. In addition, by opening an account to use the GO-EXCHANGE.COM Platform:</p>" +
                          "<ul>" +
                          "<li>you have accepted these Terms; and</li>" +
                          "<li>you are confirming that you are at least 18 years of age and have the full capacity to accept these Terms and enter into a transaction as a result of using the GO-EXCHANGE.COM Platform</li>" +
                          "<li>you agree only to trade with legally obtained funds that belong to you</li>" +
                          "<li>you agree to take full responsibility for your trading or non-trading actions and any gains or losses sustained as a result of using the GO-EXCHANGE.COM platform</li>" +
                          "<li>you confirm the details provided upon registration are true and accurate</li>" +
                          "<li>you are confirming that you are not residing in, nor a citizen of, the United States of America</li>" +
                          "<li>you agree to abide by any relevant laws in your jurisdiction, including reporting any trading profits for taxation purposes.</li>" +
                          "</ul>" +
                          "<p>Important</p>" +
                          "<p>Persons located in certain territories, currently including the United States of America and its territories and the countries specified as prohibited, excluded or similar as part of the terms and conditions and/or list on the website, in each case as supplemented from time to time are not permitted to use the website, with the exception of withdrawing any funds directly to the account owner. To remove any doubt, this restriction also applies to residents and citizens of other nations while located in a prohibited jurisdiction. The fact that the website is accessible in a prohibited jurisdiction, or that the website allows the use of the official language of a prohibited jurisdiction, shall not be construed as a license to use the website in such prohibited jurisdiction. any attempt to circumvent this restriction, for example, by using a vpn, proxy or similar service that masks or manipulates the identification of your real location, or by otherwise providing false or misleading information regarding your citizenship, location or place of residence, or by using the website through a third party or on behalf of a third party located in a prohibited jurisdiction is a breach of this agreement. If it becomes apparent, or we have reasonable grounds to suspect, that you are located in any of the prohibited jurisdictions, this may result in closing your account, without an obligation to provide you with advance notice, all in accordance with applicable laws and agreements.</p>" +
                          "<p>Changes to Terms</p>" +
                          "<p>Go-Exchange reserves the right, at its sole discretion, to change, add or remove portions of these Terms, at any time. Such notification will be made via the Website. It is your responsibility to review the amended Terms. Your continued use of the Website and Platform following the posting of changes will mean that you accept and agree to the changes and you agree that all subsequent transactions by you will be subject to these Terms.</p>" +
                          "<p>Go-Exchange reserves the right to close inactive accounts that have exhibited no events or transaction activity for a period greater than 6 months. Customers of whom accounts will be closed as a result of this condition will be contacted by Go-Exchange using the customers registered email address 14 days prior to account closure. After the account is closed, customers will have 30 days to make arrangements for any funds held in the account or notify Go-Exchange by email that they wish to keep the account open. Go-Exchange will not be liable for any funds that were held within the account after 30 days from account closure.</p>",
      'VAULT_HEAD2':      'Account Balance at a Glance',
      'VAULT_HEAD3':      'Check Account Activities',
      'VAULT_HEAD4':      'Send and Receive',
      'VAULT_HEAD5':      'Other Features',
      'VAULT_HEAD7':      'World Market Data',
      'VAULT_HEAD8':      'Debit Card Order and Recharge',
      'VAULT_TEXT1':      'The all-in-one mobile wallet to manage your EGDs',
      'VAULT_TEXT2_H1':   'Quick View',
      'VAULT_TEXT2_1':    "View your latest portfolio value at current exchange rates.",
      'VAULT_TEXT2_H2':   'Portfolio View',
      'VAULT_TEXT2_2':    "View all your assets in one single comprehensive listing.",
      'VAULT_TEXT2_H3':   'Multi-Currencies Support',
      'VAULT_TEXT2_3':    "View all the fiat currencies you currently hold.",
      'VAULT_TEXT2_H4':   "Base Currency Setting",
      'VAULT_TEXT2_4':    "Select the home currency you want your account to be based in.",
      'VAULT_TEXT3_H1':   "Transaction List",
      'VAULT_TEXT3_1':    "View latest transactions on your account with details such as date, time and exchange rates.",
      'VAULT_TEXT3_H2':   "Pending Transactions",
      'VAULT_TEXT3_2':    "Pending transactions are listed on a separate tab with real time updates on the number of confirmations received before the transaction is fully completed.",
      'VAULT_TEXT4_H1':   "Send EGDs",
      'VAULT_TEXT4_1':    "To send EGDs, simply enter the amount of EGDs and the recipient's EGD wallet address or QR code. If the person does not currently own a EGD wallet, you can just enter their email address!",
      'VAULT_TEXT4_H2':   "Receive EGDs",
      'VAULT_TEXT4_2':    "Save the EGD addresses of your friends and family into your address book and send them EGDs any time you want.",
      'VAULT_TEXT5_H1':   "Alert Setup",
      'VAULT_TEXT5_1':    "Set up alerts so that you get email notifications when your desired price is reached!",
      'VAULT_TEXT5_H2':   "Address Book",
      'VAULT_TEXT5_2':    "Save the EGD addresses of your friends and family into your address book and send them EGDs any time you want.",
      'VAULT_TEXT6_1':    "Get this mobile wallet now, available on:",
      'VAULT_TEXT7_H1':   "World Markets",
      'VAULT_TEXT7_1':    "Get real time market data from the world’s most popular EGD exchanges. The information is accessible to everyone and does not require an Go-exchange account.",
      'VAULT_TEXT8_H1':   "Order Cards",
      'VAULT_TEXT8_1':    "Choose your preferred debit card and delivery option then place your order.",
      'VAULT_TEXT8_H2':   "Recharge Cards",
      'VAULT_TEXT8_2':    "To add funds to your card, simply enter the recharge amount in EGD and it will be transferred directly into your debit card."
  });

}]);

/**
 * Created by albert on 9/1/14.
 */

angular.module('anxApp.pages').config(['$translateProvider', function ($translateProvider) {

  $translateProvider.translations('zh_CN', {
    'DISCLAIMER':       '虚拟货币的价格波动，一天内能大幅上落。买卖虚拟货币可涉及重大风险，请注意并评估当中相关的风险及损失。 Go-Exchange不会推销虚拟货币作为投资工具。交易虚拟货币的决定权最终取决于用户的独立判断。',
    'SITE_NAME':        'Go-Exchange',
    'HEAD_ABOUT':       '关於Go-Exchange',
    'HEAD_ADV':         'Go-Exchange的优势',
    'HEAD_BUYSELL':     '购买EGD',
    'HEAD_CARD':        'EGD签帐金融卡',
    'HEAD_CRYPTO':      '购买EGD',
    'HEAD_VAULT':       'EGD，随身随心',
    'HEAD_STORE':       '',
    'FOOTER_VAULT':     'EGD钱包',
    'ABOUT_TEXT1_1':    "<p>Go-Exchange.COM 提供简单丶安全和低收费的平台，让用户们自由买卖和交换虚拟电子货币—EGD。 我们提供24小时的网上交易服务，给注册用户提供自动化的买卖指令配对服务。Go-Exchange.COM 让您轻松交易虚拟货币，立志成为您跟EGD之间连接的桥梁!</p>" +
    "<p>母公司（Go-Exchange）总部设在香港，成立於2013年7月并依法注册。交易引擎利用LMAX干扰器技术，该技术在投资银行外汇兑交易中广泛使用。该引擎的处理设计为高容量,高吞吐量和低延迟交易。所有货币统一结算。</p>" +
    '<h5 class="branded">简易</h5>' +
    "<p>Go-Exchange.COM 致力提供简单却细致的用户界面，让您对您的交易一览无遗。</p>" +
    "<p>我们的开发人员会定期举办小组研讨会，收集用户对界面的要求及体验建议。我们首要考虑的因素就是让用户使用轻松简洁的页面。</p>" +
    "<p>Go-Exchange.COM 开发人员具有低延迟软件开发方面的丰富经验，配对引擎可以让用户在买卖的时候享受到耳目一新的感觉。</p>" +
    "<p>配 对引擎能预先提供市场参考价格，使用户们能够根据自己的喜好选择特定数量的EGD或是以特定价格完成交易， 此配对引擎已被证实除了能在亚毫秒间执行各种指令，也能支持每日数以百万计交易量，其稳定性是毋庸置疑的。所以 Go-Exchange.COM 能在变幻莫测的市场中迅速、有效率和可靠地让您进行交易。</p>" +
    "<p>Go-Exchange.COM 客户服务团队在营运和安保方面也训练有素、经验十足，最重要的是—我们是绝对忠诚的团队，我们准备好随时为您服务，并能解答任何您可能会遇到的问题。</p>" +
    "<p>我 们的客户服务指引(Key Performance Indicator, KPI)要求在咨询提出后的12小时内给予用户回应和解答；但是在大部份的情况下，我们都会第一时间回答您。我们对客户服务的承诺是Go-Exchange.COM 员工的企业文化。客户的建议对于我们来说是十分重要的，所以我们将会按 Go-Exchange.COM 用户们的要求，不定期的推出新功能。</p>" +
    '<h5 class="branded">安保</h5>' +
    "<p>安保对于 Go-Exchange.COM 是极为重要的。</p>" +
    "<p>安 保对于 Go-Exchange.COM 是极为重要的。我们运作的每一个步骤都经过有系统的设计以提供最佳的安全性。其中包括检查所有物理侵入的因素、详尽的审批和对我们工作人员的背景检查、虚 拟货币被储存在冷藏库中，所有提款要求都经过人手（当然是非常有效率的）进行处理，并小心谨慎的留意任何潜在的安全漏动，包括社交工程、网络钓鱼和远程 0day攻击。</p>" +
    "<p>我们的员工接受专门的训练，能够识破可疑的交易和要求，并在任何程序上都完全遵守KYC和AML的措施。</p>" +
    "<p>如果您希望知道更多有关网站的安全性，请查阅常见问题内的安全和技术部份。</p>" +
    "<p>我们一直力求符合监管机构的要求，并与金融公司合作，共同提供灵活性的存款和提款选择。不论是本地或是全球的监管机构，我们确保完全达到其监管的要求，使其对我们建立信任，令 Go-Exchange.COM 能够长期成功地营运下去。</p>" +
    '<h5 class="branded">低收费</h5>' +
    "<p>Go-Exchange.COM 希望在EGD市场中提供最低价的服务费用。</p>" +
    "<p>这能够改善流通性和收窄息差，最重要的是这能增加您的潜在盈利，我们亦会定期推出优惠给所有用户。</p>",
    'ABOUT_TEXT2_0':    '<div class="info">Mouseover profile images to find out more about each individual</div>',
    'ABOUT_TEXT2_1':    '<div class="profile-title">行政总裁及联合创办人</div>' +
    '<div class="profile-name">卢建邦先生</div>' +
    '<p><ul>' +
    '<li>具有丰富经历的企业管理顾问，尤其资深於商业战略策划执行方面</li>' +
    '<li>在商业发展销售和市场营销策略拥有超过15年的经验</li>' +
    '<li>在国际领先的大型全球性企业，参与丶领导及部署项目管理</li>' +
    '<li>最近更参与国际规模电信企业於亚太地区和中东/非洲地区的区域性合规，风险，治理以及法规等管理</li>' +
    '</ul></p>',
    'ABOUT_TEXT2_2':    '<div class="profile-title">首席技术总监及联合创办人</div>' +
    '<div class="profile-name">马登希先生</div>' +
    '<p><ul>' +
    '<li>金融市场方面的专家，在创建和管理机构和零售金融服务的技术管理以及架构低延迟交易平台方面拥有15年的经验</li>' +
    '<li>开源性代码和网站安全方面的专家</li>' +
    '<li>最近参与监管汇丰投资银行整个全球外汇交易架构</li>' +
    '</ul></p>',
    'ABOUT_TEXT2_3':    '<div class="profile-title">首席营运总监及联合创办人</div>' +
    '<div class="profile-name">查曼迪</div>' +
    '<p><ul>' +
    '<li>在银行技术和法规监管方面拥有超过10年的资深经验，尤其擅长交易环境分析和相关系统的落实</li>' +
    '<li>拥有法规及监管的资深背景</li>' +
    '<li>最近参与管理汇丰投资银行机构交易市场的技术工作</li>' +
    '</ul></p>',
    'ABOUT_MORE_2':     "Go-Exchange的优势",
    'ADV_SUBHEAD':      "为何我们是你跟EGD的联系？",
    'ADV_1_1':          "<p>安保对於 Go-Exchange.COM 是极为重要的。以下是我们用以使您安心的几个元素：<ul>" +
    "<li>支持三重身份验证</li>" +
    "<li>离线备份冷储存</li>" +
    "<li>分布式安全机制</li>" +
    "<li>不会使用客户名下资金进行其它用途</li>" +
    "<li>使用行业领先的DDoS防护系统服务供应商</li>" +
    "<li>企业级多重防火墙</li>" +
    "<li>服务器托管在Tier 3+ ISO 27001/9001认证的标准数据中心</li></ul>" +
    "<p>如果您想了解更多关於我们的安保规定，请点击这里。</p>",
    'ADV_2_1':          "<p>Go-Exchange致力於提供一个安全，全球兼容及信誉良好的服务给您。我们为业务的诚信及透明度引以为傲。" +
    "<ul><li>Go-Exchange是全球都认可的品牌，在EGD交易平台中排名世界第六 。</li>" +
    "<li>在香港依法注册，拥有香港海关发出的金钱服务经营者牌照系统(MSO)。 也是亚洲第一家拥有此牌照的交易平台。</li>" +
    "<li>Go-Exchange拥有强大的创业及管理层团队，成员均拥有金融市场，管理咨询，银行技术和法规方面的背景， 更是在使用规范以及法规方面拥有资深经验。是 KYC (了理客户)和AML(反洗黑钱)方面的专家。</li></ul></p>",
    'ADV_3_1':          "<p>我们不断努力提高流动性并收紧差价，使能在EGD市场为你提供最优惠的价格，确保你的潜在利润能最大限度地提高。" +
    "<ul><li>在全球EGD交易平台中排名世界第六</li>" +
    "<li>全球唯一能在世界上最大的两个EGD交易市场 - 中国和美国处理EGD交易的平台 。</li>" +
    "<li>低差价，高流动性</li></ul></p>",
    'ADV_4_1':          "<p>Go-Exchange客户服务团队在营运和安保方面训练有素丶经验十足，最重要的是—我们是绝对忠诚的团队，我们准备好随时为您服务，并能解答任何您可能会遇到的问题。我们提供:" +
    "<ul><li>支持多种语言 (英语,简体,繁体中文及葡萄牙文)</li>" +
    "<li>我们的客户服务指引(Key Performance Indicator, KPI)要求在咨询提出後的12小时内给予用户回应和解答</li></ul>",
    'ADV_5_1':          "<p>您无需为使用Go-Exchange交易平台购买EGD而支付交易费。购买EGD的价格取决於在您提交订单时於Go-Exchange经销商网络上正在活动的用户们。所显示的价格为总价，您的账户将被按照屏幕上显示的详情相应地存入和扣除资金。" +
    "<ul><li>零交易费</li>" +
    "<li>零银行存款费</li>" +
    "<li>零EGD提现费</li>" +
    "<li>没有任何的隐藏费用</li></ul>" +
    "<p>详情请参阅收费表。</p>",
    'ADV_8_1':          "<p>支持使用全球5种法定货币购买EGD:</p>" +
    "",
    'BUYSELL_SUBHEAD': "前所未有的轻松购买EGD",
    'BUYSELL_TEXT1_H1': "购买",
    'BUYSELL_TEXT1_1': "提供您零距离管理EGD的捷径，您无需为了购买EGD而去了解买入、卖出或者不同种类的订单类型。",
    'BUYSELL_TEXT2_H1': "购买EGD",
    'BUYSELL_TEXT2_1': "用任何Go-Exchange支持的充值方式或者使用即时购买功能，输入您需要购买的EGD数量接着点击“下订单”，您就能为您的Go-Exchange账户充值。从您账户中扣除的总价会停留显示30秒以让您确认您的购买。如果你在30秒内无法确认交易，我们会为您显示更新后的价格。一旦确认成功，EGD就会直接传输到您的Go-Exchange钱包中。您的EGD可以被立即取出，您也可以把您的EGD存入您想要存入的钱包中。",
    'BUYSELL_TEXT2_H2': "",
    'BUYSELL_TEXT2_2': "",
    'BUYSELL_TEXT2_3': "为了保证提供您最可观的价格，Go-Exchange引入了专为Go-Exchange交易平台度身定做的Go-Exchange经销商网络(Go-Exchange Dealer Network)。当提交一份购买或者出售的订单后，Go-Exchange交易平台会迅速接收来自Go-Exchange经销商网络上所有已注册的买家或卖家的匹配订单。Go-Exchange交易平台能即时挑选出最优惠的价格，在您每次提交订单的时候都会在屏幕上停留显示30秒钟。",
    'BUYSELL_TEXT2_4': "您无需再为使用Go-Exchange交易平台购买EGD而支付交易费。购买EGD的价格取决于在您提交订单时于Go-Exchange经销商网络上正在活动的用户们。所显示的价格为总价，您的账户将被按照屏幕上显示的详情相应地存入和扣除资金。没有任何的隐藏费用。",
    'BUYSELL_TEXT3_2': "我们使用三重验证系统，提供额外的安保措施以防止未经授权人士入侵您的账户。我们把资金存在离线备份冷储存中，以保证您的EGD能够安全的储存在您的Go-Exchange钱包中。",
    'BUYSELL_TEXT3_3':  "支持如全球银行电汇、本地银行转账、自动柜员机及店内现金存款。",
    'BUYSELL_TEXT3_H4': "支持多种货币与EGD的交易",
    'BUYSELL_TEXT3_4': "支持使用全球5种法定货币为您的账户充值: 美元，欧元，英镑，加元和人民币。",
    'BUYSELL_TEXT4_2': "现在就于Go-Exchange开始购买EGD!",
    'BUYSELL_TEXT5_1': "如果您想要于特地的价格和次数内交易您的比特支持多种货币与EGD的交易币，可以尝试我们广受好评的ANXPRO交易平台。支持山寨币与其他虚拟货币的交易，是一个能处理您所有虚拟货币的专业平台。只需使用您现有的Go-Exchange账户即能登陆Go-ExchangePRO，真正做到一个账户享受两种专业服务！",
    'CAREERS_TEXT1_1':  "<p>EGD正在改变世界，而这个过程已经在逐步开始。如果您也希望参与EGD世界，成为它的一份子，我们期待能听到业界最有热诚的人的声音。请向我们介绍一下您自己，告诉我们您能提供什麽才能给我们，您可以直接发简历至应聘电邮至<a href='mailto:careers@go-exchange.com'>careers@go-exchange.com</a></p>",
    'CAREERS_TEXT2_1':  "Forum Manager | 2014-08-08 | Permanent<br>Front End Developer | 2014-08-08 | Permanent<br>UX Developer | 2014-08-08 | Permanent<br>",
    'CRYPTO_TEXT1_1':   "能够让您购买EGD和其他虚拟货币的多功能交易平台。该平台对新手用户来说十分简单易用，对资深用户来说又十分专业。只需使用一个账户就能同时享用Go-Exchange和Go-ExchangePRO的专业服务。",
    'CRYPTO_TEXT2_H1':  "支持对等网络的虚拟币",
    'CRYPTO_TEXT2_1':   "EGD于2009年1月推出，是最为著名的对等网络虚拟货币。EGD于2008年1月由一位匿名人士以中本聪的化名推出，是首个由用户完全自治理，无需中央管理或中间人的对等网络的支付系统。从用户的角度来看，EGD就像是互联网上的现金。欲了解更多关于EGD的资料，请访问我们的FAQ页面。",
    'CRYPTO_TEXT2_2':   "莱特币于2011年10月推出，在EGD已有的基础上进行改进。但是莱特币总量与其竞争者—EGD相比起来，能够生产更大的货币量（8400万）。莱特币每2.5分钟处理一个数据块，比EGD每10分钟处理一个数据块更频密。莱特币也使用其他POW证明，采用scrypt算法。以及拥有专业的ASIC矿机。",
    'CRYPTO_TEXT2_3':   "狗狗币于2013年12月推出，一经推出就制定了在2014年底产出高达1000亿狗狗币的生产计划，之后每年也计划增加52亿狗狗币。狗狗币的诞生引爆了互联网，是一枚爆炸型的虚拟货币。",
    'CRYPTO_TEXT2_4':   "域名币于2011年4月推出，被认为是EGD的首个分身。作为后补的一种虚拟货币，它也能作为另外一种分散的域名系统，避免域名遭到审查。",
    'CRYPTO_TEXT2_5':   "点点币于2012年8月被推出，被认为混合了权益证明/工作量证明系统的虚拟货币。不像其他虚拟货币，点点币在挖矿的数量上没有硬性的限制，取而代之的是有限的的发行率加上内置1%的分散的通货膨胀。",
    'CRYPTO_TEXT2_6':   "恒星币於2014年8月被推出，是一个非营利性组织-恒星币团队的一部分，致力於通过对等网络执行虚拟币与法定货币的兑换以及其本身作为虚拟币的显着特点，鼓励公众更广泛地采用虚拟货币。",
    'CRYPTO_TEXT2_7':   "",
    'CARD_SUBHEAD':     "一卡在手，通行全球",
    'CARD_TEXT_Q':      '有问题吗? 请与<a href="mailto:support@go-exchange.com">support@go-exchange.com</a>联络',
    'CARD_TEXT1_0': "* 此图片仅作示范，实际的EGD签帐金融卡与此图片设计会有所不",
    'CARD_TEXT1_H1': "轻松自如随时随地把EGD转移到Go-ExchangeEGD签帐金融卡!",
    'CARD_TEXT1_1': "自动连接至您的Go-Exchange交易账户，方便的存款到您的EGD签帐金融卡中让您每天都能使用",
    'CARD_TEXT2_2': "可以在任何时候任何地点提取现金，这张卡能够在全球超过2500万个地点使用",
    'CARD_TEXT2_H3': "销售终端机",
    'CARD_TEXT2_3': "在全球任何角落都能通过销售终端机购物",
    'CARD_TEXT2_H4': "全球网络购物",
    'CARD_TEXT2_4': "用它在网络世界大血拼",
    'CARD_TEXT2_H5': "向书面手续丶繁琐的步骤和信用检查说不！",
    'CARD_TEXT3_1': "立即登入或免费注册订购您的EGD签帐金融卡",
    'CARD_TEXT3_2':     "立即<a href=\"/\">登入</a>或<a href=\"/register\">免费注册</a>订购您的EGD签帐金融卡",
    'CARD_TEXT4_H1':    "比现金更安全",
    'CARD_TEXT4_1':     "如果您的金融卡遗失或被盗，您可停用它并将馀额转移至补发的新卡。不会像遗失现金那样一无所有。",
    'CARD_TEXT4_H2':    "比现金更方便",
    'CARD_TEXT4_2':     "购物时不用手忙脚乱地点算现金丶外游时也可跟兑换外币说不，只需使用您的金融卡!",
    'CARD_TEXT4_H3':    "无限的灵活性",
    'CARD_TEXT4_3':     "让您选择金融卡的货币，并在参与商户享受优惠。",
    'CARD_TEXT4_H4':    "全球通用",
    'CARD_TEXT4_4':     "可于全球自动柜员机网络提取现金或随时随地于世界各地购物。",
    'CARD_TEXT4_H5':    "轻松理财",
    'CARD_TEXT4_5':     "您可透过网上理财或手机检查您的Go-Exchange金融卡帐户资料及馀额。",
    'CARD_TEXT4_H6':    "简单申请",
    'CARD_TEXT4_6':     "申请金融卡非常容易，无须通过书面手续丶繁琐的步骤或信用审查。",
    'CARD_TEXT5_H1':    "特色金融卡",
    'CARD_TEXT5_1':     "Go-ExchangeEGD签帐金融卡，让您与EGD时刻紧扣，尽享各式各样的精采优惠和便捷。Go-Exchange为您提供了一系列的金融卡选择，确保所选的都能迎合您的生活所需。",
    'CARD_TEXT5_H2':    "EGD尊享卡",
    'CARD_TEXT5_2':     "<ul><li>以港币结算</li>" +
    "<li>可用于网上签账</li>" +
    "<li>网上帐户管理</li>" +
    "<li>无需月费</li></ul>",
    'CARD_TEXT5_H3':    "Go-Exchange 至尊卡",
    'CARD_TEXT5_3':     "<ul><li>以美元结算</li>" +
    "<li>无限额现金提取</li>" +
    "<li>无购物签账费用</li>" +
    "<li>可用於网上签账</li></ul>",
    'CARD_TEXT5_4':     'We offer special pricing on bulk orders (minimum of 10 cards), for more details please contact <a href="mailto:support@go-exchange.com">support@go-exchange.com</a>',
    'CARD_TEXT6_H1':    "如何运作",
    'CARD_TEXT7_H1':    "申请金融卡",
    'CARD_TEXT7_1':     "登入或注册网上申请金融卡",
    'CARD_TEXT7_H2':    "充值",
    'CARD_TEXT7_2':     "把EGD充值到金融卡",
    'CARD_TEXT7_H3':    "使用",
    'CARD_TEXT7_3':     "使用您的金融卡提取现金或随时随地签账购物",
    'CARD_TEXT7_H4':    "充值",
    'CARD_TEXT7_4':     "通过网上理财充值金融卡",
    'CARD_TABLE1_H1':   "功能",
    'CARD_TABLE1_H2':   "Go-Exchange 普通卡",
    'CARD_TABLE1_H3':   "Go-Exchange 至尊卡",
    'CARD_TABLE1_H4':   "Go-Exchange 尊享卡",
    'CARD_TABLE1_H5':   "限额",
    'CARD_TABLE1_H6':   "收费表",
    'CARD_TABLE1_H7':   "帮助与支援",
    'CARD_TABLE1_H8':   "优惠与奖赏",
    'CARD_TABLE1_R1_1': "金融卡结算货币",
    'CARD_TABLE1_R1_2': "美元",
    'CARD_TABLE1_R1_3': "美元",
    'CARD_TABLE1_R1_4': "港元",
    'CARD_TABLE1_R2_1': "无需信用审查",
    'CARD_TABLE1_R3_1': "自动柜员机提款",
    'CARD_TABLE1_R4_1': "销售终端机购物",
    'CARD_TABLE1_R5_1': "网上签账",
    'CARD_TABLE1_R6_1': "手机应用程序",
    'CARD_TABLE1_R6_3': "即将推出",
    'CARD_TABLE1_R7_1': "网上账户管理",
    'CARD_TABLE1_R7_3': "即将推出",
    'CARD_TABLE1_R7_4': "即将推出",
    'CARD_TABLE1_R8_1': "快速充值",
    'CARD_TABLE1_R8A_1': "国际商户网络",
    'CARD_TABLE1_R8B_1': "循环充值",
    'CARD_TABLE1_R9_1': "金融卡最高馀额",
    'CARD_TABLE1_R10_1':"最高充值额",
    'CARD_TABLE1_R11_1':"自动柜员机提款",
    'CARD_TABLE1_R11_3':"视乎所使用之柜员机限额",
    'CARD_TABLE1_R11_4':"视乎所使用之柜员机限额",
    'CARD_TABLE1_R12_1':"销售终端机购物",
    'CARD_TABLE1_R13_1':"网上签账",
    'CARD_TABLE1_R14_1':"自动柜员机提款",
    'CARD_TABLE1_R14_4':"1%, 最低US$3",
    'CARD_TABLE1_R15_1':"销售终端机购物",
    'CARD_TABLE1_R16_1':"自动柜员机查询馀额",
    'CARD_TABLE1_R16_3':"免费",
    'CARD_TABLE1_R17_1':"月费",
    'CARD_TABLE1_R17_2':"免费",
    'CARD_TABLE1_R17_3':"免费",
    'CARD_TABLE1_R17_4':"首12个月免费，<br>第13个月起收取HK$5。<br>费用从卡的余额中扣除",
    'CARD_TABLE1_R18_1':"充值费",
    'CARD_TABLE1_R18_2':"US$5 + 充值金额的2.5%",
    'CARD_TABLE1_R18_3':"免费",
    'CARD_TABLE1_R18_4':"免费",
    'CARD_TABLE1_R19_1':"常见问题解答",
    'CARD_TABLE1_R20_1':"电话",
    'CARD_TABLE1_R21_1':"电邮",
    'CARD_TABLE1_R22_1':"迎新优惠",
    'CARD_TABLE1_R22_2':"将金融卡增值至所需金额可获运费退还",
    'CARD_TABLE1_R22_3':"增值8,000港元",
    'CARD_TABLE1_R22_4':"增值5,000美元",
    'CARD_TABLE1_R23_1':"推荐给好友",
    'CARD_TABLE1_R23_2':"推荐朋友成功获取金融卡可获现金奖赏",
    'CARD_TABLE1_R24_1':"一对一升级",
    'CARD_TABLE1_R24_2':"现有Go-Exchange普通卡持卡人可免费将金融卡升级",
    'CARD_TABLE1_R25_1':"商户优惠",
    'CARD_TABLE1_R25_2':"于参与商户可获专享优惠",
    'CARD_TABLE1_R26_1':"假日优惠",
    'CARD_TABLE1_R26_2':"于2015年1月31日或之前成功验证您的帐户可免费获赠Go-Exchange尊享卡",
    'CARD_TABLE1_R27_1':"选用特快挂号收取验证码可获免费将金融卡升级到Go-Exchange至尊卡",
    'CARD_TABLE1_R28_1':"",
    'CONTACT_TEXT_H1':  '客戶服務',
    'CONTACT_TEXT_1':   '<p>Go-Exchange.COM 客户服务团队在营运和安保方面也训练有素、经验十足，最重要的是—我们是绝对忠诚的团队，我们准备好随时为您服务，并能解答任何您可能会遇到的问题。</p>' +
    '<p>我们的客户服务指引(Key Performance Indicator, KPI)要求在咨询提出后的12小时内给予用户回应和解答；但是在大部份的情况下，我们都会第一时间回答您。我们对客户服务的承诺是Go-Exchange.COM 员工的企业文化。客户的建议对于我们来说是十分重要的，所以我们将会按Go-Exchange.COM 用户们的要求，不定期的推出新功能。</p>' +
    '<p>Go-Exchange.COM客服为周一至周五全天候服务。我们也会在周一尽快回复于上周末收到的电邮。如有任何问题及查询都可以联络:<a href="mailto:support@go-exchange.COM">support@go-exchange.COM</a></p>',
    'CONTACT_TEXT_H2':  '社交网络渠道',
    'CONTACT_TEXT_H3':  'Go-Exchange总部',
    'CONTACT_TEXT_3':   'Go-Exchange INTERNATIONAL',
    'CONTACT_TEXT_4':   "香港湾仔告士打道200号<br>新银集团中心7楼701室<br>星期一至星期五: 9:00am - 6:00pm",
    'DISCLAIM_TEXT_1':  "<p>阁下有责任确定在收取或执行一些付款时而产生的税项（如适用），也有责任收集，汇报和向有关部门提交正确的税务资料。 Go-Exchange不会负责阁下在任何交易上产生的有关税项，也不会向有关部门或第三方机构收集，汇报和提交因这些交易而产生的税项。</p>" +
    "<p>购买EGD，虚拟货币和虚拟商品绝不适合绝大部分人士，因为买卖时需要承受较高风险。在决定买卖之前，阁下应该慎重考虑投资目的，经验和能够承受风险的程度。在决定投资前，阁下应该清楚此投资有可能导致部分损失或全部损失，所以应该以能承受的损失程度来决定投资的金额。阁下应该注意到虚拟商品衍生来的风险，如有任何疑问，建议先寻求理财顾问的协助。此外，除了上述提及过的风险以外，还会有未能预测的风险存在。阁下请慎重考虑并用清晰的判断能力去评估自己的财政状况才作出任何买卖虚拟商品的决定。</p>" +
    "<p>任何意见丶消息丶探讨丶分析丶价格和其他资讯在此网站上的均是一般的市场评论，并不构成投资建议。Go-Exchange不会承担任何因依赖这些资讯，无论是直接或间接而产生的任何利润损失。</p>" +
    "<p>使用互联网形式的交易系统也存有风险包括来自于软件、硬件和互联网连接失败产生的风险。由于Go-Exchange不会控制互联网的可靠性和可用性，因此Go-Exchange不会对失真，延误和连接失败导致的损失负责。</p>" +
    "<p>此网站的内容会随时更改并不作另行通知，Go-Exchange已采取合理措施确保网站资讯的准确性，但并不保证其准确性程度，亦不会接受任何因为网站上的资讯或因未能连接互联网丶传送或接收任何通知和信息时的延误或失败而直接或间接造成的损失。</p>" +
    "<p>此网站以英文版本为准。</p>",
    'FEES_TEXT_0':      "<p>您无需再为使用Go-Exchange交易平台买卖网络黄金（EGD）而支付交易费。买卖网络黄金（EGD）的价格取决於在您提交订单时於Go-Exchange经销商网络上正在活动的用户们。所显示的价格为总价，您的账户将被按照屏幕上显示的详情相应地存入和扣除资金。如欲了解更多关於Go-Exchange经销商网络的详情，请点击这里.</p>" +
    "<p>Go-Exchange没有任何的隐藏费用。存款及提款费用将以以下列方式收取:</p>",
    'FEES_TEXT_H1':     "存款及提款费用",
    'FEES_TEXT_1':      '<table class="table table-condensed">' +
    "   <thead>" +
    "       <tr>" +
    "           <th>供资方式</th>" +
    "           <th>费用</th>" +
    "       </tr>" +
    "   </thead>" +
    "   <tbody>" +
    "       <tr><td>通过Sepa存款(欧元)</td><td>免费</td></tr>" +
    "       <tr><td>通过银行汇款(欧元)</td><td>存款金额的1%</td></tr>" +
    "       <tr><td>通过银行汇款(美元)</td><td>存款金额的1%</td></tr>" +
    "       <tr><td>通过银行汇款(加元)</td><td>存款金额的1%</td></tr>" +
    "       <tr><td>通过银行汇款(英镑)</td><td>存款金额的1%</td></tr>" +
    "       <tr><td>EGD存款</td><td>免费</td></tr>" +
    "       <tr><td>EGD提款</td><td>免费（不包括虚拟币网络费）</td></tr>" +
    "   </tbody>" +
    "</table>" +
    "<p>在某些情况下，您的银行或其他第三方中介机构可能会收取电汇操作的额外费用。这些费用是由您的银行或第三方中介机构决定收取的并与Go-Exchange无关。</p>",
    'FEES_TEXT_H2':     "转账限额",
    'FEES_TEXT_2':      '<table class="table table-condensed">' +
    '   <thead>' +
    '       <tr>' +
    '           <th>账户状态</th>' +
    '           <th>最低法定货币存款</th>' +
    '           <th>最高法定货币存款</th>' +
    '           <th>最低签账金融卡充值</th>' +
    '           <th>最高签账金融卡充值</th>' +
    '           <th>虚拟币存款</th>' +
    '           <th>虚拟币提款</th>' +
    '       </tr>' +
    '   </thead>' +
    '   <tbody>' +
    '       <tr><td>未經驗證</td><td>不提供</td><td>不提供</td><td>不提供</td><td>不提供</td><td>不限制</td><td>20 EGD</td></tr>' +
    '       <tr><td rowspan="1">已驗證</td><td>500美元（或等值）</td><td>40,000美元（或等值）</td><td>100美元<br>800港币</td><td>40,000美元<br>200,000港币</td><td rowspan="1">不限制</td><td>700 EGD</td></tr>' +
    '   </tbody>' +
    "</table>",
    'FOOTER_ABOUT':     'Go-Exchange',
    'KYC_TEXT_1':       "<p>KYC（了解你的客户）和AML（反洗黑钱）政策</p>" +
    "<p>Go-Exchange致力于向它的客户提供安全的，全球兼容的，有信誉的服务。我们非常自豪的提供完整和透明的业务，因为Go-Exchange全面贯彻履行KYC（了解你的客户）以及AML（反洗黑钱）政策。</p>" +
    "<p>Go-Exchange 总部设在香港，是一家合法注册的公司。Go-Exchange坚持全面贯彻KYC（认识您的客户）和AML（反洗黑钱）合规政策。包括可疑交易监管和向当地监管机构及其他合规机构强制性报告等。</p>" +
    "<p>我们的AML和KYC政策会根据不同国家和地区的不同客户而有不同的进一步的处理。具体分析到每个区域的专门的AML和KYC政策，请参考如下信息。我们稳健的合规框架坚持符合本地和全球的监管要求，以求提供诚信服务和确保Go-Exchange持续稳定的运营。</p>" +
    "<p>Go-Exchange有权拒绝以下人士的注册：不符合国际反洗黑钱标准的司法管辖区内人士或被视为政治敏感的人士。</p>" +
    "<p>Go-Exchange有职责和权利对客户展开调查，用于验证我们的用户和他们交易的权利。加强客户审查，是作为我们进行的风险和评估的一部分。除了这一点，任何企图滥用Go-Exchange或其平台将导致帐户立即冻结，并报告给有关当局。</p>" +
    "<p>Go-Exchange 的AML和KYC政策如下</p>" +
    "<ul>" +
    "<li>通过Go-Exchange平台进行的交易将会受到反洗黑钱的交易监管。</li>" +
    "<li>身份证明文件、地址证明文件和交易记录的证明文件将会维持至少六年，倘若被提交给监管部门，恕不另行通知注册用户。</li>" +
    "<li>Go-Exchange可在任何时间暂停您的帐户的操作，并不向您透露有关方面原因的责任。 Go-Exchange会根据此项条款行使其权利后再通知您。</li>" +
    "<li>注册会员的交易若涉及法定货币，将会被要求核实其身份，并遵守Go-Exchange的KYC政策。这包括同时提交身份证明和地址证明的核证副本。有效身份证明文件包括公民身份证及护照。地址有效证明文件，包括以发出日期起计不超过3个月的水电煤单和税单。其他形式的身份证明文件和地址证明文件将不会被接受。您可以在账户验证成功后使用账户进行交易。</li>" +
    "<li>可疑的交易活动报告会提交给香港特别行政区联合财富情报组（ “情报组” ）。</li>" +
    "</ul>",
    'PARTNER_TEXT_1':   "<p>无论是个人或机构都可成为Go-Exchange的合作伙伴。我们提供不同的合作方案，查询有关详情请电邮至<a href=\"mailto:support@go-exchange.com\">support@go-exchange.com</a></p>",
    'PRIVACY_TEXT_1':   '<p>根据香港特别行政区《个人资料（私隐）条例》（「私隐条例」）而作出， 目的是向阁下阐明收集个人资料的原因、用途和查询个人资料记录的途径。</p>' +
    '<p><strong>为什麽要收集个人资料？</strong></p>' +
    '<p>我们收集阁下的资料是为了提供服务，提升客服和改善用户在我们网站上的使用经验。我们会在有关的表格和申请上明确地列出那些资料是强制性提供或是自愿性提供的；如提供资料是属强制性质，阁下必须提供有效个人资料来完成对有关服务的申请。而在自愿性质的情况下，阁下可以向我们提出不希望个人资料在自愿性用途上。阁下亦可匿名地在网站上浏览。</p>' +
    '<p>如阁下未满18岁（未成年），你必须请父母或监护人发出同意提供个人资料同意书。</p>' +
    '<p><strong>我们会如何使用你的信息？</strong></p>' +
    '<p>我们不会出售，交换或以其他方式转让阁下的个人资料给外方，但我们收取得来的信息会用作以下其中一种的用途：</p><ul>' +
    '<li>个性化您的体验(您的信息能帮助我们更加有效地回应您的个人需要)</li>' +
    '<li>改善我们的网站(我们一直致力於完善网站，并根据您的信息和意见进行改良)</li>' +
    '<li>改善客服(您的信息能帮助我们更有效率地回应客服要求和支援需求)</li>' +
    '<li>处理交易(您的信息在没有得到您的同意之前，不论是公开和私人的，均不会因任何原因而出售、分享、转移或给予任何其他公司。除非是为了明确目的的产品购买和服务请求。)</li>' +
    '<li>发送定期电邮(在订单处理时所提供的电邮，或会用作发送信息或向您更新订单的状态，此外还会定期收到公司的最新消息、更新、相关产品或服务信息等。)</li>' +
    '<li>为了符合KYC（认识您的客户）和AML（反洗黑钱）合规框架 （当您的帐户进行法定货币交易，我们需要验證您的身份以及获得更多的资料）</li></ul>' +
    '<p><strong>我们会否把阁下的资料发到第叁方？</strong></p>' +
    '<p>本公司会绝对把你的信息保密，但我们有可能将阁下的资料发到以下部门（无论是在香港特别行政区以内或是以外）：<ol></p>' +
    '<li>专业顾问，服务供应商，承包商和代理商，提供行政，电脑，内容，支付服务，营销和所有与我们业务有关的供应商；该供应商需要展示出他们有适当的政策，程序和措施来保护由我们透露的资料。</li>' +
    '<li>金融卡用户的资料会被提交到金融卡发卡商以保持业务运作。</li>' +
    '<li>在根据法律业务上的要求，我们会把无论是个人或机构的资料提供至相关部门。</li></ol>' +
    '<p><strong>我们会把阁下的信息用作直接营销吗？</strong></p>' +
    '<p>我们不会把阁下的资料用作直接营销或将阁下的资料发到第叁方作为直接营销用途。</p>' +
    '<p><strong>我们会如何保护你的信息？</strong></p>' +
    '<p>我们会实行不同的安保措施，以在您下订单或输入、提交或存取私人信息时让您的个人信息得以安全保管。我们会采用安保服务器。所有提供的敏感信息将会透过安全通讯端层(SSL) 技术进行传输，然後再加密至我们的数据库中，只有特别授权许可的人士才能进行我们的系统，并需要保密处理所有信息。</p>' +
    '<p><strong>我们会使用cookies吗？</strong></p>' +
    '<p>是的，（Cookies是细小的档案，用於网站或其服务供应商透过网络浏览器快速传输信息至您的浏览器中(如您允许的话)，这能让服务供应商辨识您的浏览器并捕捉和记下特定讯息。)我们会使用cookies 以帮助我们记下和处理您在购物车内的项目，并了解和储存您在将来浏览中可能的优先意愿。</p>' +
    '<p>大部分的浏览器都会预设为允许cookies。阁下可以在设定上选取‘不允许’，但更改了设定後，某些功能可能会无法正常运作。</p>' +
    '<p><strong>第叁方链接</strong></p>' +
    '<p>偶尔，在我们的酌情决定下，我们可能会在网站上刊登或提供第叁方的产品和服务。这些第叁方网站拥有单独的私隐政策。所以我们没有责任或义务对有关这些连结网站的内容和活动负责。然而，我们诚挚致力於寻求保护网站，并乐意收到任何有关这些网站的意见和建议。</p>' +
    '<p><strong>更改隐私政策</strong></p>' +
    '<p>如果我们决定更改我们的私隐政策，我们会在此页面作出有关更改。</p>' +
    '<p><strong>保留数据</strong></p>' +
    '<p>我们会根据香港特别行政区的个人资料（私隐）条例来用作保留阁下资料的指引，没有用途的个人资料会将被摧毁。</p>' +
    '<p><strong>资料查询／更改请求</strong></p>' +
    '<p>根据该条例，任何人有权要求查阅本公司持有其个人资料及更正的权利。按照该条例的规定，本公司有权就处理任何查阅资料的要求收取合理费用。</p>',
    'PRIVACY_TEXT_2':   '',
    'SECURITY_TEXT1_1': "Go-Exchange的基石，为您提供一份安心。",
    'SECURITY_TEXT1_2': "",
    'SECURITY_TEXT1_3': "所有的客户资金采用分布式安全机制，而且我们采用离线备份冷藏库存储资金，以防止盗窃和丢失。<br>我们不会使用客户名下资金进行其它用途；所有资金都会频繁核对。此外，所有资金可由第三方持牌审计进行年度审核。",
    'SECURITY_TEXT2_H1':"三重身份验证",
    'SECURITY_TEXT2_1': "两步过程进行登录，<br>三个步骤核查提款。",
    'SECURITY_TEXT2_2': "<p>提供额外的安保措施以防止未经授权人士入侵您的账户。</p>" +
    "<p>除了普通密码，你会被要求输入由Google Authenticator所产生一个实时的密码。为您提供额外的安全保障，当您提交提款申请，你需要激活验证电子邮件中的链接以确认该指令。</p>",
    'SECURITY_TEXT3_H1':"SSL 加密",
    'SECURITY_TEXT3_1' :"您跟我们网站之间的通讯均受到128位元的加密处理，如网上银行活动一样得到同等水平的保护。这种类型的加密对於保密通讯是世界各地的可信标准。",
    'SECURITY_TEXT3_H2':"密码加密",
    'SECURITY_TEXT3_2' :"MD5即消息摘要算法第五版，是已知较弱的存储密码散列值。我们不会使用MD5以散列您的密码。当然，我们也不会使用同等水平的非保安程序。即使我们的数据库被盗用，骇客也需要知道密码散列的解密法才能成功登入您的帐户。",
    'SECURITY_TEXT3_H3':"国家级别的先进基础架构",
    'SECURITY_TEXT3_3' :"Go-Exchange.COM平台设於第三级别的数据中心服务器上。我们不会使用云端供应商存储数字货币。",
    'SECURITY_TEXT3_H4':"DDOS 保护",
    'SECURITY_TEXT3_4' :"我们选用世上最强的保护形式以对抗分布式拒绝服务的攻击。我们不想假装能做到这一点，但是我们与多个第三方的合作夥伴都能证实减轻互联网史上的某些大型DDOS攻击。",
    'SECURITY_TEXT3_H5':"数据库安全",
    'SECURITY_TEXT3_5' :"我们的系统已设计成能够防止SQL注入式攻击。每小时的备份，我们会加密发送完整数据库和实时钱包的备份至场外的後备地点，其中会处於随时待命的状态。如有需要，我们能够回复数据库至任何攻击之前的状态，这让我们能够取消可疑的交易活动，并在最少的停机时间中尽我们所能复原帐户和资金结馀，并恢复运作。",
    'SECURITY_TEXT3_H6':"企业级多重防火墙",
    'SECURITY_TEXT3_6' :"我们采用一个非常严格的方式对所有进入和外向的信息流进行密切监察，以确保能避免恶意攻击以及防止网络被入侵或任何对数据的威胁。",
    'SECURITY_TEXT4_H1':"停机应变计划",
    'SECURITY_TEXT4_1' :"如发生任何不幸的停机或受攻击情况，我们将会与您在 Go-Exchange.COM 帐户上所登记的电邮地址保持联络，并在有需要时给您提供後备运作的新网址。我们的冷藏库措施将确保您主要的资金能得到安全保管，并在我们恢复运作时可供使用。",
    'SECURITY_TEXT4_H2':"定期的压力和浸泡测试",
    'SECURITY_TEXT4_2' :"我们的技术是能够随即扩展的。我们的定期压力测试已证实其达至亚10微秒的处理程序，且我们的浸泡测试在24小时间已测试超过1千万宗交易。换算: 我们的引擎和底层基础架构能够处够负荷和处理大量的交易。",
    'SECURITY_TEXT5_H1':"合规性框架",
    'SECURITY_TEXT5_1' :"Go-Exchange坚守完整和贯彻的KYC (了理客户) 和AML (反洗黑钱) 合规性框架。这包括了监督可疑的交易和义务地汇报给本地的监管机构和其他合规机构。",
    'SECURITY_TEXT5_2' :"我们的AML 和KYC 措施视乎客户的居住国家会有所不同，并通过 Go-Exchange.COM 的注册程序进一步纪录在案。我们的特定措施会详列於使用条款中，而您作为新用户在经过注册程序时亦必需同意它。我们的稳健合规框架能确保符合当地和国际水平的监管要求，达致一定水平的信任，并确保 Go-Exchange.COM 能永久地营运下去。",
    'STORE_SUBHEAD': "西营盘分行<br>香港",
    'STORE_TEXT1_1': "香港西营盘干诺道西118号11C<br>t +852 2548 2078<br>f +852 2892 2635<br>support@go-exchange.com",
    'STORE_TEXT1_2': "营业时间：<br>星期一至星期五: 11:00am - 6:00pm",
    'STORE_TEXT1_3': "交通指引与地图",
    'STORE_TEXT2_H1': "提供服务:",
    'STORE_TEXT2_H2': "身份验证",
    'STORE_TEXT2_H3': "现金存款",
    'STORE_TEXT2_H4': "购买EGD",
    'TERMS_TEXT_H1': "条款",
    'TERMS_TEXT_H2': "KYC和AML",
    'TERMS_TEXT_H3': "免责声明",
    'TERMS_TEXT_1': "<p>使用本网站将被视为您已阅读并同意以下使用条款：</p>" +
    "<p>这些条款和条件（“条款”）所载的条件下，根据香港法律注册成立的公司Go-Exchange（以下简称“Go-Exchange”）为您提供使用Go-Exchange.COM网站https://www.Go-Exchange.COM（以下简称“网站”）和访问Go-Exchange.COM平台（“平台”）的权限。请仔细阅读这些条款，请不要使用本网站或平台，除非你接受他们。</p>" +
    "<p>此外，下列术语适用于这些条款和条件及任何或所有协议：“客户”，“注册会员”，“您”和“您的”是指给您，这个网站的访问，并接受本公司的条款和条件的人。 “本公司”，“我们”和“我们自己”，是指Go-Exchange.COM。 “当事人”，“各方”，或“我们”，是指以客户和我们自己，或客户或自己。所有条款是指要约，承诺，并考虑必要承接Go-Exchange.COM和其相关联的平台所提供的服务，付款或费用。</p>" +
    "<p>Go-Exchange.COM和其相关平台促进服务，而注册会员可以以法定货币交换虚拟货币。</p>" +
    "<p>通过注册和使用任何由Go-Exchange.COM的网站或平台提供的功能，您将被视为已阅读，理解并接受以下所有条款及条件。此外，通过开放使用的帐户Go-Exchange.COM平台：</p>" +
    "<ul>" +
    "<li>您已接受这些条款;</li>" +
    "<li>您确认您至少年满18岁，并有充分的能力接受这些条款，并订立交易，使用Go-Exchange.COM平台</li>" +
    "<li>您同意只交易合法取得属于你的资金。</li>" +
    "<li>您同意您的交易或非交易行动承担全部责任和任何收益或亏损。</li>" +
    "<li>您确认注册时提供的细节是真实和准确</li>" +
    "<li>您同意遵守任何有关法律的规定，就税务目的而言，包括报告任何交易利润。</li>" +
    "</ul>" +
    "<p>条款的变更</p>" +
    "<p>在其自行决定，更改，添加或删除这些条款的部分，在任何时候，Go-Exchange.COM保留一切权利。所有通知将通过网站发出。参考修订条款是您的责任。如果您继续使用本网站和平台公布的变化将意味着您接受并同意更改，您同意您的所有后续交易将受到这些条款影响。</p>" +
    "<p>对于一些在6个月以上未有任何交易或活动的账户，Go-Exchange会保留有关权利去删除这些户口。此类型的账户持有人将会在户口被删除的14天前收到Go-Exchange发出的通知电邮。当户口被删除后，客户仍有30天去处理户口内的结余或可以以电邮形式通知Go-Exchange保留此账户。在户口关闭后的30天，Go-Exchange将不会为户口内的结余负上任何责任。</p>",
    'VAULT_HEAD2': '帐户结馀一目了然',
    'VAULT_HEAD3': '查询账户记录',
    'VAULT_HEAD4': '发送和接收',
    'VAULT_HEAD5': '其他功能',
    'VAULT_HEAD7':      '世界市场资讯',
    'VAULT_HEAD8':      '订购及增值金融卡',
    'VAULT_TEXT1': '高效便捷集所有功能於一身的EGD钱包',
    'VAULT_TEXT2_H1': '账户概览',
    'VAULT_TEXT2_1': "查看按现行汇率计算的账户总值",
    'VAULT_TEXT2_H2': '综合资产概览',
    'VAULT_TEXT2_2': "在单一综合列表检视您所有的资产。",
    'VAULT_TEXT2_H3': "支持多种货币",
    'VAULT_TEXT2_3': "查看您目前帐户内持有的所有法定货币。",
    'VAULT_TEXT2_H4': "基本货币设置",
    'VAULT_TEXT2_4': "选择您想要计算帐户总值的货币种类",
    'VAULT_TEXT3_H1': "查询账户记录",
    'VAULT_TEXT3_1': "查看您帐户内的最新交易及详情，如日期，时间和汇率。",
    'VAULT_TEXT3_H2': "等待处理的交易",
    'VAULT_TEXT3_2': "正在等待处理的订单会在另一个列表上显示，并会即时更新目前所收到的确认指令直至交易完成。",
    'VAULT_TEXT4_H1': "发送EGD",
    'VAULT_TEXT4_1': "只需输入EGD的数量和收件人的EGD钱包地址或二维码(QR Code)。如果此人目前尚未拥有EGD钱包，你只需输入他的电邮地址！",
    'VAULT_TEXT4_H2': "接收EGD",
    'VAULT_TEXT4_2': "您只需向发送者提供您的EGD钱包地址或二维码(QR Code)然後检查您的帐户!",
    'VAULT_TEXT5_H1': "通知设置",
    'VAULT_TEXT5_1': "当到达您想要的EGD价格时你将收到电邮通知！",
    'VAULT_TEXT5_H2': "地址簿",
    'VAULT_TEXT5_2': "保存您的家人和朋友的EGD地址到地址簿中，随时随地都可发送EGD。",
    'VAULT_TEXT6_1': "立即下载",
    'VAULT_TEXT7_H1':   "世界市场",
    'VAULT_TEXT7_1':    "紧贴全球主要EGD交易所的市场数据，实时更新EGD的最新价格。",
    'VAULT_TEXT8_H1':   "订购金融卡",
    'VAULT_TEXT8_1':    "选择您喜欢的金融卡及运送方式然後进行订单。",
    'VAULT_TEXT8_H2':   "增值金融卡",
    'VAULT_TEXT8_2':    "只需以EGD输入您的充值金额, 资金将直接转移到您的金融卡。"
  });

}]);

angular.module('app.simpleTrade', ['ngRoute', 'templates', 'anxApp.login', 'anxApp.simpleTrade']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/trade', {
    templateUrl: 'simple_trade/trade.html'
  });
}]).run(["RoutePermissionsService", function(RoutePermissionsService) {
  return RoutePermissionsService.requireAuth('/trade');
}]);



angular.module('app.trade', ['anxApp.trader']).config(["$routeProvider", function($routeProvider) {
  return $routeProvider.when('/trade', {
    templateUrl: 'trader/trader.html'
  });
}]).run(["RoutePermissionsService", function(RoutePermissionsService) {
  return RoutePermissionsService.requireAuth('/trade');
}]);

angular.module('app.translations', ['anxApp.translations']);

angular.module('app.translations').config(["LanguageServiceProvider", function(LanguageServiceProvider) {
  return LanguageServiceProvider.registerLanguage('en_US', {
    SITE_TITLE_ANXPRO: 'ANXPRO Bitcoin Exchange'
  });
}]);

angular.module('anxApp.translations').config(['LanguageServiceProvider', function(LanguageServiceProvider) {
	LanguageServiceProvider.registerLanguage('en_US', {"actions.count.heading":"3","actions.start.heading":"Getting Started","actions.1.heading":"Get Verified","actions.1.text":"After verifying your identity, you will be free to deposit funds into your account","actions.1.href":"/settings/verify","actions.2.heading":"Fund Your Account","actions.2.text":"You can transfer EGD you already own into your account, or if you don't have any, you can easily buy some right here! All you have to do is deposit funds to any one of our bank accounts.","actions.2.href":"/funds","actions.3.heading":"Purchase EGD","actions.3.text":"Once you have funded your account, you can start buying EGD!","actions.3.href":"/trade","actions.4.heading":"Account Maintenance","actions.4.text":"We recommend you set a strong password to protect your account. You may even want to set up 2-Factor Authentication if you want to be extra secure. Also, complete our simple online KYC form and enjoy the full freedom of a verified account.","actions.4.href":"/settings"});

	LanguageServiceProvider.registerLanguage('en_US', {"footer.label.copyright":"Copyright © Go-Exchange 2015","footer.label.disclaimer":"DISCLAIMER: Crypto currencies prices are volatile and fluctuate day-to-day. Trading in these crypto currencies may be considered a high risk activity. Proper and sound judgement should be used in evaluating the risks associated with these activities. Go-Exchange does not solicit nor make any representation that crypto currencies are an investment vehicle. The decision to trade crypto currencies rests entirely on the users own independent judgement."});

	LanguageServiceProvider.registerLanguage('en_US', {"heading.services.buysell.label":"Purchase EGD","heading.deposit.instructions.label":"<p>Your deposit requests and their current status are listed below. Please note the following:</p><ul><li>All deposits are subject to Go-Exchange KYC and AML policies.</li><li>Fiat deposits are regularly processed every weekday between the hours of 9am – 6pm Hong Kong Local time (GMT+8).</li><li>Crypto deposits are processed automatically after 6 Blockchain confirmations (STR is processed immediately).</li></ul>","heading.depositnotification.label":"<p>Your deposit requests and their current status are listed below. Please note the following:</p>\r\n<ul>\r\n<li>All deposits are subject to Go-Exchange KYC and AML policies.</li>\r\n<li>Local HKD Currency bank transfer deposits do not incur any fees. </li>\r\n<li>Local AUD Currency bank transfer deposits do not incur any fees. </li>\r\n<li>Local CHF Currency bank transfer deposits incur Fr {0} CHF + {1} % of deposit amount as processing fee. </li>\r\n<li>SEPA(Single Euro Payments Area) EUR Currency bank transfer deposits incur € {2} EUR  + {3} % of deposit amount as processing fee. </li>\r\n<li>International Bank Wire Deposits do not incur any fees. Additional fees may be charged by your bank or third party intermediary. Go-Exchange will not be liable for any fees deducted from your deposit by your bank or third party intermediary. </li>\r\n<li><a href=\"https://www.egopay.com/\" target=\"_blank\" class=\"alert-link\">EgoPay</a> deposits incur the processing fee as <B>2.5%</B> of deposit amount.</li>\r\n<li><a href=\"https://www.zipzapinc.com/\" target=\"_blank\" class=\"alert-link\">ZipZap</a> GBP deposits incur £ {4} GBP + {5} % of deposit amount as processing fee. </li>\r\n<li><a href=\"https://www.zipzapinc.com/\" target=\"_blank\" class=\"alert-link\">ZipZap</a> USD deposits incur $ {6} USD + {7} % of deposit amount as processing fee. Please note that ZipZap <strong>USD</strong> deposits are only supported in <strong>Brazil</strong> for the moment. Please kindly <strong>specify amount in USD</strong> on our website but <strong>pay corresponding Brazilian Real</strong> amount (converted according to exchange rate set up by ZipZap) at payment spot according to instructions in your payment slip. </li>\r\n<li>Deposits are regularly processed every weekday between the hours of 9am – 6pm Hong Kong Local time (GMT+8).</li>\r\n</ul>\r\n"});

	LanguageServiceProvider.registerLanguage('en_US', {"help.buyatfixed.label":"Buy at fixed price","help.sellatfixed.label":"Sell at fixed price","help.buyatmarket.label":"Buy at market price","help.sellatmarket.label":"Sell at market price","help.connectionerror.text":"We're experiencing data connection issues. Please try reloading the site later.","help.connectionlost.header":"Lost connection with server","help.connectionlost.text":"Displayed data may not be up-to-date and certain actions may not be available.","help.buysell.text":"<li>Select the unit of measure, either EGD or your base currency from the dropdown.</li><li>Enter the amount of EGD you want to buy.</li><li>Click on the Quote Price button and our system will provide you with the best quoted price from our open dealer network.</li><li>You will be provided with a quotation for your transaction. The quote will be valid for a specified timeframe, which upon expiry will be refreshed.</li><li>Confirm your transaction.</li>","help.funds.general.text":"<p>The widgets below allow you to make deposit and withdraw notifications for your account. Click on the help icons for further contextual information. A full fees schedule can be found <a href=\"/pages/fees\" target=\"_blank\">here</a>.</p><p>Please check the <a href=\"/activity\" data-toggle=\"tab\">Activity</a> page to view your current deposit and withdrawal requests as well as other transactional data related to your account.</p>","help.tips.buysell.text":"Purchasing EGD is easy! Once you have funded your account, you can use the Purchase panel above to start buying or selling EGDs. More instructions are provided above.","help.tips.advance.text":"If you would like to trade altcoins or are looking for advanced trading, please try ANXPRO, our exchange trading platform for crypto-currencies. Just click on the button below!","help.funds.paymentproviders.text":"We currently provide integration with the following payment services. If you wish to use any one these services and are doing so for the first time, please complete the initial application by following the on-screen instructions."});

	LanguageServiceProvider.registerLanguage('en_US', {"nav.label.buysell":"Purchase","nav.label.card":"Cash Out"});

	LanguageServiceProvider.registerLanguage('en_US', {"simpletrade.error.general.tryagain":"An error occurred processing your request. Please try again later.","simpletrade.error.general.notavaliable":"Your request cannot be completed at the moment","simpletrade.error.INSUFFICIENT_LIQUIDITY":"Insufficient liquidity"});
}]);
angular.module('anxApp.translations').config(['LanguageServiceProvider', function(LanguageServiceProvider) {
	LanguageServiceProvider.registerLanguage('zh_CN', {"actions.1.heading":"验证您的账户","actions.1.text":"一旦验证您的账户, 您就可轻松存款现金到我们的平台。","actions.2.heading":"存款到您的账户","actions.2.text":"您可将现时持有的EGD存入你的帐户。如你并没持有任何EGD，你只需存款到我们的任何一个银行账户然后轻松地通过我们的交易平台购买EGD。","actions.3.heading":"购买EGD","actions.3.text":"一旦您存款现金到我们平台，就可开始购买EGD！","actions.4.heading":"账户维护","actions.4.text":"我们建议您设置一个强密码来保护您的帐户。如您想增强安全性，您甚至可以设置双重认证。此外，请填妥我们简单的KYC（了解客户）表格，享受核实您的账户后带给您的自由。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"footer.label.copyright":"版权所有 © Go-Exchange 2015","footer.label.disclaimer":"免责声明：虚拟货币的价格波动，一天内能大幅上落。买卖虚拟货币可涉及重大风险，请注意并评估当中相关的风险及损失。 Go-Exchange不会推销虚拟货币作为投资工具。交易虚拟货币的决定权最终取决于用户的独立判断。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"heading.services.buysell.label":"购买EGD"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"help.buysell.text":"<li>于下拉式选单选择EGD或您的基准货币，您可选择使用自选的法定货币或EGD为单位。</li><li>输入你想买入的EGD数量。</li><li>点击报价。系统会从开放式经销商网络为您提供现时最优惠的价格。</li><li>您将获得一个报价。此报价会在指定的时间内有效，逾时将被刷新。</li><li>请确认交易。</li>","help.tips.buysell.text":"购买EGD非常容易！当存款到您的帐户后，您可使用上面的购买操作板开始购买EGD。在上文中我们有提供详细说明。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"nav.label.buysell":"购买","nav.label.card":"兑现"});
}]);
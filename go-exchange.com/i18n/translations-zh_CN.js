angular.module('anxApp.translations').config(['LanguageServiceProvider', function(LanguageServiceProvider) {
	LanguageServiceProvider.registerLanguage('zh_CN', {"actions.1.heading":"验证您的账户","actions.1.text":"一旦验证您的账户, 您就可轻松存款现金到我们的平台。","actions.2.heading":"存款到您的账户","actions.2.text":"您可将现时持有的EGD存入你的帐户。如你并没持有任何EGD，你只需存款到我们的任何一个银行账户然后轻松地通过我们的交易平台购买EGD。","actions.3.heading":"购买EGD","actions.3.text":"一旦您存款现金到我们平台，就可开始购买EGD！","actions.4.heading":"账户维护","actions.4.text":"我们建议您设置一个强密码来保护您的帐户。如您想增强安全性，您甚至可以设置双重认证。此外，请填妥我们简单的KYC（了解客户）表格，享受核实您的账户后带给您的自由。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"footer.label.copyright":"版权所有 © Go-Exchange 2015","footer.label.disclaimer":"免责声明：虚拟货币的价格波动，一天内能大幅上落。买卖虚拟货币可涉及重大风险，请注意并评估当中相关的风险及损失。 Go-Exchange不会推销虚拟货币作为投资工具。交易虚拟货币的决定权最终取决于用户的独立判断。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"heading.services.buysell.label":"购买EGD"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"help.buysell.text":"<li>于下拉式选单选择EGD或您的基准货币，您可选择使用自选的法定货币或EGD为单位。</li><li>输入你想买入的EGD数量。</li><li>点击报价。系统会从开放式经销商网络为您提供现时最优惠的价格。</li><li>您将获得一个报价。此报价会在指定的时间内有效，逾时将被刷新。</li><li>请确认交易。</li>","help.tips.buysell.text":"购买EGD非常容易！当存款到您的帐户后，您可使用上面的购买操作板开始购买EGD。在上文中我们有提供详细说明。"});

	LanguageServiceProvider.registerLanguage('zh_CN', {"nav.label.buysell":"购买","nav.label.card":"兑现"});
}]);